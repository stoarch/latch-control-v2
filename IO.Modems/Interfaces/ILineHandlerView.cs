﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 29.08.2018
 * Time: 12:22
 * 
 */
using System;

namespace FosforLatch.Interfaces
{
	/// <summary>
	/// Description of ILineHandlerView.
	/// </summary>
	public interface ILineHandlerView
	{
		void SetReceivedOk();
		void SetReceivedError();
	}
}

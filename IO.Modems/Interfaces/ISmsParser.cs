﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 05.09.2018
 * Time: 12:53
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace IO.Modems.Interfaces
{
	/// <summary>
	/// Description of ISmsParser.
	/// </summary>
	public interface ISmsParser
	{				
		void ParseSms(string smsNo, string sms);		
	}
}

﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 04.09.2018
 * Time: 10:16
 * 
 
 */
using System;
using System.Collections.Generic;

namespace IO.Modems.Interfaces
{
	/// <summary>
	/// Description of ISmsReporter.
	/// </summary>
	public interface ISmsReporter
	{
		void SetNoMoreSms();
		void SetSmsReceived();
		void SetSmsSent();
		void SetSmsHandled();
		void ClearSmsStatus();	
	}
}

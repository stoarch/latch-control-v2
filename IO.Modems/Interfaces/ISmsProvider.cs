﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 01.09.2018
 * Time: 13:03
 * 
 */
using System;
using IO.Modems.Interfaces;

namespace FosforLatch.Interfaces
{
	public delegate void SmsEventHandler(string smsNo, string smsText);
	
	/// <summary>
	/// Description of ISmsProvider.
	/// </summary>
	public interface ISmsProvider
	{
		event SmsEventHandler SmsReceived;
		
		void RemoveAllSms();
		void SendSms(string smsNo, string smsText);
		
		ISmsParser SmsParser {get;set;}
	}
}

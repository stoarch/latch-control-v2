﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 30.04.2018
 * Time: 10:37
 * 
Bugs:
	TODO: Handle error for "AT+CMGS="+77010315418"| | > reset| +CMGS: 3| | OK| | +CMTI: "SM",1| | "
	TODO: Handle error for "AT+CMGR=14| | +CMGR: "REC UNREAD","+77010315418","","18/09/24,12:26:34+24"| 0| | OK|" => "| 0"
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Threading;

using FosforLatch.Interfaces;
using IO.Modems.Interfaces;
using Tools.Interfaces;

namespace FosforLatch
{
	/// <summary>
	/// Send and receive/parse modem commands and responses (AT/SMS)
	/// </summary>
	public class Modem : ISmsProvider
	{
		const string NEW_SMS_RESPONSE = ">";
		
		const string SMS_SENT_RESPONSE = "CMGS:";
		const string SMS_RECEIVED_RESPONSE = "CMTI:";
		const string SMS_READEN_RESPONSE = "CMGR:";

		const string OK_RESPONSE = "OK";
		const string ERROR_RESPONSE = "ERROR";
		
		const int MAX_SMS = 17;

		private bool needToParseSmsContents_ = false;
		private bool dataReceived_ = false;

		List<string> smsContents_ = new List<string>();

		List<string> atCommands_ = new List<string>();
		int curCommand = -1;
		
		List<string> smsCommands_ = new List<string>();
		int curSmsCommand = -1;

		string fullReceivedLine_ = "";

		string receivedLine_;
		DateTime sendTime_;
		bool needToRemoveAllSms_ = false; //set this flag if next read should delete all sms
		bool noMoreSms_ = false;
		
		string receivedSmsNum_;
		
		enum SmsState
		{
			Unknown,
			NewSms,
			SmsSent,
			SmsReceived,
			SmsReaden,
			SmsHandled
		}
		
		private SmsState smsState_ = SmsState.Unknown; 

		public Modem()
		{
		}
		
		public SerialPort Port { get; set; }
		
		public string ReceivedLine 
		{ 
			get
			{ 
				return receivedLine_; 
			}
			set
			{
				receivedLine_ = value;
			}
		}
		
		public string FullReceivedLine
		{
			get
			{
				return fullReceivedLine_;
			}
			set
			{
				fullReceivedLine_ = value;
			}
		}
		
		public DateTime SendTime { get{ return sendTime_; } }
		public ISmsParser SmsParser {get;set;}
		public ISmsReporter SmsReporter {get;set;}
		public ILogger Logger {get;set;}
		public ReceivedLineHandler LineHandler { get; set; }
		
		public void SendToSerial(string msg)
		{
			if(Port == null)			
			{
				Debug.Print("Port is undefined");
				return;	
			}			
			
			if(!Port.IsOpen)
			{
				LogText("Serial port closed");
				return;
			}
			
			sendTime_ = DateTime.Now;
			receivedLine_ = "";
			
			dataReceived_ = false;
			Port.Write(msg);
			Debug.WriteLine("Written " + msg );
		}
		
		void LogText(string message)
		{
			Console.WriteLine(message);
			if(Logger != null)
				Logger.LogText(message);
		}
		
		void SetSmsSent()
		{
			if(SmsReporter != null)
				SmsReporter.SetSmsSent();
		}
		
		void SetSmsReceived()
		{
			if(SmsReporter != null)
				SmsReporter.SetSmsReceived();		
		}

		public void HandleReceived(string msg)
		{
			ReceivedLine += msg;
			
			if(ReceivedLine.Contains(Environment.NewLine))
			{
				LogText(ReceivedLine);
				
				if( needToParseSmsContents_ )
				{
					smsContents_.Add(ReceivedLine);
				}
								
				HandleReceivedLine();
				
				fullReceivedLine_ += ReceivedLine.RemoveCarriageReturn('|');
				ReceivedLine = "";

				LogText("Full received: " + fullReceivedLine_);
				
				
				//sms can arrive at any moment - handle it
				if(fullReceivedLine_.Contains(SMS_RECEIVED_RESPONSE))
				{
					SetSmsReceived();
					smsState_ = SmsState.SmsReceived;			

					HandleReceivedSms();

					if(needToRemoveAllSms_)//clear sms buffer (modem has 20 sms only)
					{
						RemoveAllSms();
						needToRemoveAllSms_ = false;
					}
				}						
				
				if(fullReceivedLine_.Contains(SMS_READEN_RESPONSE))
				{
					ParseSMSReadenResponse();			
				}
			}
			
			switch (smsState_) {
				case Modem.SmsState.Unknown:
					if(fullReceivedLine_.Contains(NEW_SMS_RESPONSE)||receivedLine_.Contains(NEW_SMS_RESPONSE))
					{
						LogText("SMS START");
												
						SendNextSmsCommand();
						smsState_ = SmsState.NewSms;
					}					
					break;
					
				case Modem.SmsState.NewSms:
					if(fullReceivedLine_.Contains(SMS_SENT_RESPONSE))
					{
						SetSmsSent();
						
						smsState_ = SmsState.SmsSent;
					}			
					break;
					
				case Modem.SmsState.SmsSent: //do nothing
					break;
					
				case Modem.SmsState.SmsReceived:
					if(fullReceivedLine_.Contains(SMS_READEN_RESPONSE))
					{
						string readen = "";
						
						if( smsContents_.Count > 0 )
							readen = smsContents_[0];
						else
							readen = FullReceivedLine;
						
						receivedSmsNum_ = ParseSmsHeader(readen);
						
						smsContents_.Clear();
						needToParseSmsContents_ = true;
						smsState_ = SmsState.SmsReaden;
					}					
					break;
					
				case SmsState.SmsReaden:
					smsState_ = SmsState.SmsHandled;
					break;
					
				case Modem.SmsState.SmsHandled:			
					smsState_ = SmsState.Unknown; //reset to next sms					
					break;
					
				default:
					throw new Exception("Invalid value for SmsState");
			}			
		}
		
		void SetSmsHandled()
		{
			if(SmsReporter != null)
				SmsReporter.SetSmsHandled();			
		}
		
		void ParseSMSReadenResponse()
		{
			if(fullReceivedLine_.Contains(SMS_READEN_RESPONSE))
			{
				int pos = fullReceivedLine_.IndexOf("| | OK|");
				if(pos < 0)
				{
					LogText("Partial received CMGR");
					
					return;
				}
				string response = fullReceivedLine_.Substring(pos - 3, 3);
				LogText(response);
				
				//LINE: AT+CMGR=1| | +CMGR: "REC READ","+77022136653","","18/09/05,11:17:17+24"| 101| | OK| 
				//TODO: Parse sms from this				
				receivedSmsNum_ = ParseSmsHeader(fullReceivedLine_);
				
				ParseSmsResponse(response);			
				
				SetSmsHandled();
			}
		}
		
		void ParseSmsResponse(string response)
		{
			if(SmsParser != null)
				SmsParser.ParseSms(receivedSmsNum_, response);
		}
		
		void HandleReceivedSms()
		{
			//Response should contain: +CMTI: "SM",1
			if(!fullReceivedLine_.Contains(SMS_RECEIVED_RESPONSE))
			{
				LogText("Unable to handle sms in text: " + ReceivedLine);				
				
				return;
			}
			
			int qi = fullReceivedLine_.IndexOf("\"SM\",");
			if( qi == -1 )
			{
				LogText("Error: Unable to find sms no");
				
				return;
			}
			
			qi += 5;
			
			int lastCR = fullReceivedLine_.LastIndexOf('|');
			
			if(lastCR < qi)
				return;
			
			string smsNo = fullReceivedLine_.Substring( qi, lastCR - qi );			

			if (string.IsNullOrEmpty(smsNo)) {
				LogText("Empty smsNo received");
				return;
			}
			
			LogText("SMS at " + smsNo);
						
			if(smsNo.Contains("|"))			
			{
				int barPlace = smsNo.IndexOf('|');
				smsNo = smsNo.Substring(0, barPlace - 1);
				
				LogText("SmsNo after removing |" + smsNo );
			}
			
			noMoreSms_ = false;
			
			int smsNumber = Convert.ToInt16(smsNo); 
			ReadSmsAt(smsNumber);
			
			
			if(smsNumber > MAX_SMS)
			{
				needToRemoveAllSms_ = true;
			}
		}
		
		public void ReadSmsAt( int smsIndex )
		{
			if(noMoreSms_)
			{
				
				return;//do nothing - no sms in modem at current time
			}
			
			atCommands_.Clear();
			atCommands_.Add("AT+CMGR=" + smsIndex.ToString() + Environment.NewLine);
			StartCommandSend();			
		}
		
		void HandleReceivedLine()
		{
			if(ReceivedLine.Contains(OK_RESPONSE))
			{
				HandleOkResponse();
			}
			else if(ReceivedLine.Contains(ERROR_RESPONSE))
			{
				HandleErrorResponse();
			}			
		}

		public void SendNextSmsCommand()
		{			
			curSmsCommand += 1;	
			if( curSmsCommand >= smsCommands_.Count )
			{
				LogText("No more sms commands");
								
				return;
			}

			SendToSerial( smsCommands_[curSmsCommand] );
			
			ClearSmsStatus();
		}
		
		void ClearSmsStatus()
		{
			if( SmsReporter != null )
				SmsReporter.ClearSmsStatus();			
		}

		public string ParseSmsHeader(string header)
		{
			//todo: Parse it from response
			//  +CMGR: "REC UNREAD","+77022136653","","18/04/23,11:16:38+24"
			int firstCommaId = header.IndexOf(',');
			int secondCommaId = header.IndexOf(',',firstCommaId + 1);
			
			if(firstCommaId < 0)
				return "";
			
			if(secondCommaId < firstCommaId)
				return "";
			
			string num = header.Substring(firstCommaId+2, secondCommaId - firstCommaId - 3 );

			return num;
		}

		public void HandleOkResponse()
		{
			if(needToParseSmsContents_)
			{
				if(smsContents_.Count > 1)
				{
					LogText("SMS Contents: " + smsContents_[1]);	
				}
			
				needToParseSmsContents_ = false;
				ParseSmsContents();			
				smsContents_.Clear();

				SetSmsHandled();
			}
			
			LogText(">>OK");			
			
			SendNextCommand();
		}	
		
		public void HandleErrorResponse()
		{
			LogText(">>Error");						
		}
		
		void ParseSmsContents()
		{
			if(smsContents_.Count < 1)
			{
				LogText("Error: Sms does not contain parsable content\n");
				
				return;
			}
			
			string response = smsContents_[0];//First line contains: 100, AU/RU, DV, AV response
					
			ParseSmsResponse(response);
		}
		
		
		
		
		void StartCommandSend(){
			curCommand = -1;
			SendNextCommand();
		}
		
		void SendNextCommand()
		{
			curCommand += 1;
			if(curCommand >= atCommands_.Count)
			{
				LogText("!! No more commands");
								
				return;
			}

			fullReceivedLine_ = ""; //we need to read all response from this command and see what's next			
			SendToSerial(atCommands_[curCommand]);
		}

		public void ReadAllSms()
		{
			atCommands_.Clear();
			atCommands_.Add("AT+CMGL=\"ALL\"\r\n");
			StartCommandSend();			
		}
		
		public void SendSms( string msg )
		{
			curSmsCommand = -1;
			ResetSmsState();
			SendSmsInternal(msg);
		}
		
		public void SendSmsWithCR( string msg )
		{
			SendSms( msg + "\r\n" );
		}
		
		public void RemoveAllSms()
		{
			noMoreSms_ = true;
			
			atCommands_.Clear();
			atCommands_.Add("AT+CMGD=1,4" + Environment.NewLine);
			StartCommandSend();
		}
		
		private string smsNumber_ = "87022136653";
		private bool configSet = false;
		
		void SendSmsInternal(string msg)
		{
			atCommands_.Clear();
			if(!configSet)
			{
				atCommands_.Add("AT+CMGF=1" + Environment.NewLine );
				configSet = true;
			}
			
			atCommands_.Add(String.Format("AT+CMGS=\"{0}\"\r\n", smsNumber_));
			
			smsCommands_.Clear();
			smsCommands_.Add(msg + (char)(26) );
			StartCommandSend();
		}		
		
		public void InitPort()
		{
			Port.DataReceived += new SerialDataReceivedEventHandler(Port_DataReceived);			
		}

		void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
		 	dataReceived_ = true;
		 	
			if(e.EventType == SerialData.Chars)
			{
				string recv = Port.ReadExisting(); 
				HandleReceived(recv);
			}
			else if(e.EventType == SerialData.Eof)
			{
				LogText(" .EOF. ");				
			}
		}
		
		public void ResetSmsState()
		{
			smsState_ = SmsState.Unknown;
		}
		
		public bool IsNewSmsState()
		{
			return smsState_ == SmsState.NewSms;
		}
		
		public void WaitUntilReceived()
		{
			const int MAX_COUNT = 30;
			
			int count = 0;
			while(!dataReceived_)
			{
				Thread.Sleep(100);
				
				if(++count > MAX_COUNT)
				{
					throw new TimeoutException("Data do not received from port");
				}
			}
		}
		
		public event SmsEventHandler SmsReceived;		
		
		void ISmsProvider.SendSms(string smsNo, string smsText)
		{
			smsNumber_ = smsNo;
			this.SendSms(smsText);
		}
	}
}

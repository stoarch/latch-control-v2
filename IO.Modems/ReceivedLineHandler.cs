﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 30.04.2018
 * Time: 10:09
 * 
 */
using System;
using FosforLatch.Interfaces;

namespace FosforLatch
{
	/// <summary>
	/// Parse and process received line responses ("OK", "ERROR")
	/// </summary>
	public class ReceivedLineHandler
	{
		const string OK_RESPONSE = "OK";
		const string ERROR_RESPONSE = "ERROR";

		public ReceivedLineHandler()
		{
		}
		
		public string ReceivedLine { get; set; }
		public ILineHandlerView LineView {get;set;}
		public Modem Modem { get; set; }
		
		public void Handle()
		{
			if(ReceivedLine.Contains(OK_RESPONSE))
			{
				Modem.HandleOkResponse();
				LineView.SetReceivedOk();
			}
			else if(ReceivedLine.Contains(ERROR_RESPONSE))
			{
				Modem.HandleErrorResponse();
				LineView.SetReceivedError();
			}			
		}
	}
}

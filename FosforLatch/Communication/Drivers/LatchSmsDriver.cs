﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 01.09.2018
 * Time: 12:49
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using FosforLatch.Communication.Interfaces;
using FosforLatch.Interfaces;
using IO.Modems.Interfaces;
using Tools.Interfaces;

namespace FosforLatch.Communication
{
	public class LatchValueEventArgs : EventArgs
	{
		public int Id {get;private set;}
		public string SmsNo {get; private set;}
		public int Value {get; private set;}
		
		public LatchValueEventArgs(int id, string smsNo, int value) : base()
		{
			Id = id;
			SmsNo = smsNo;
			Value = value;
		}
		
		public new String ToString()
		{
			return String.Format("Id: {0} SmsNo: {1} Value: {2}", Id, SmsNo, Value);
		}
	}
	
	public class LatchEventArgs : EventArgs
	{
		public int Id {get;private set;}
		public string SmsNo {get; private set;}
		
		public LatchEventArgs(int id,  string smsNo)
		{
			Id = id;
			SmsNo = smsNo;
		}

		public new String ToString()
		{
			return String.Format("Id: {0} SmsNo: {1}", Id, SmsNo);
		}
	}
	
	public delegate void LatchValueEventHandler(object sender, LatchValueEventArgs args);
	public delegate void LatchEventHandler(object sender, LatchEventArgs args);
	
	/// <summary>
	/// Send and receive sms to latch by modem
	/// 
	/// Info:
	///   1. Current version uses modem only
	///   2. Sms parsing inlined (and can be extracted)
	/// </summary>
	public class LatchSmsDriver : ILatchDriver, ISmsParser
	{
		public const string STATE_COMMAND = "state";
		public const string RESET_COMMAND = "reset";
		
		ISmsProvider smsProvider_;
		List<String> smsNums_;
		Dictionary<string, int> smsIds_;
		
		public ISmsProvider SmsProvider {
			get { return smsProvider_; }
			set 
			{
				if(smsProvider_ != null)
				{
					smsProvider_.SmsReceived -= new SmsEventHandler(smsProvider__SmsReceived);
					smsProvider_.SmsParser = null;
				}
				
				smsProvider_ = value; 
				
				if( smsProvider_ != null )
				{
					smsProvider_.SmsReceived += new SmsEventHandler(smsProvider__SmsReceived);
					
					smsProvider_.SmsParser = this;
					smsProvider_.RemoveAllSms();
				}
			}
		}

		public ILogger Log {get;set;}	
		
		public LatchSmsDriver()
		{
			PrepareSmsNums();
		}
		
		public void ReceiveState(string smsNo)
		{
			Console.WriteLine("ReceiveState:" + smsNo);
			SendSms(smsNo, STATE_COMMAND);
		}
		
		public void ResetLatch(string smsNo)
		{
			Console.WriteLine("ResetLatch:" + smsNo);
			SendSms(smsNo, RESET_COMMAND);
		}
		
		public void SendState(string smsNum, int value)
		{
			SendSms(smsNum, Convert.ToString(value));
		}

		//TODO: Move to SmsServer (after it created)
		internal void SendSms(string smsNo, string smsValue)
		{
			smsProvider_.SendSms(smsNo, smsValue);
		}

		internal string GetSmsById(int id)
		{
			return smsNums_[id];
		}
		
		internal int GetIdBySms(string sms)
		{
			int smsId;
			if(smsIds_.TryGetValue(sms, out smsId)){
				return smsId;
			}
			else
			{
				return -1;
			}
		}

		#region private methods		

		private void PrepareSmsNums()
		{
			//TODO: Move to external config or sqlite db and read from it
			smsNums_ = new List<String>();
			//OLD//
			//smsNums_.Add("+77022136653"); //Fosfor
			//smsNums_.Add("+77786415240"); //Kunaeva
			
			//NEW//
			smsNums_.Add("+77010315417"); //Fosfor
			smsNums_.Add("+77010315418"); //Kunaeva

			
			smsIds_ = new Dictionary<string, int>();
			for (int i = 0; i < smsNums_.Count; i++) 
			{
				smsIds_[smsNums_[i]] = i;
			}
		}	
		
		void smsProvider__SmsReceived(string smsNo, string smsText)
		{
			SetLatchValue(smsNo, Convert.ToInt32(smsText));
		}

		#endregion 
		
		#region ISmsDriver methods
		void ISmsParser.ParseSms(string smsNo, string response)
		{
			var nums = Regex.Split(response, @"\D+").Where(x => !string.IsNullOrEmpty(x)).ToArray();
			
			if(nums.Length == 0)
			{
				Log.LogText("No digits found in " + response);
				return;
			}
			
			string svalue = nums[0];		
			
			if(svalue == "")
			{
				Log.LogText("Empty number not processed:" + nums[0] + " from " + response);
				return;
			}
			
			int value;
			if(int.TryParse(svalue, out value))
			{
				if((value < 0)||(value > 100))
				{
					Log.LogText("Invalid value received:" + value + " must be in range [0..100]");
					return;
				}
				SetLatchValue(smsNo, value);
				return;
			}	
			
			response = response.TrimStart(' ');

			if(response == "AU")
			{
				SetAutomaticMode(smsNo);
				
				return;
			}
			
			if(response == "RU")
			{
				SetManualMode(smsNo);
				
				return;
			}
			
			if(response == "Dv")
			{
				SetDoorOpenAlarm(smsNo);
				
				return;
			}
			
			if(response == "AV")
			{
				SetControlMotorAlarm(smsNo);
				
				return;
			}
			
			if(response == "EA")
			{
				SetEAAlarm(smsNo);
				
				return;
			}
		}

		#endregion
		
		#region ILatchDriver 
		public event LatchValueEventHandler LatchStateReceived;	

		public event LatchEventHandler AutomaticModeSet;
		public event LatchEventHandler ManualModeSet;
		public event LatchEventHandler AlarmDoorOpenSet;
		public event LatchEventHandler AlarmMotorErrorSet;
		public event LatchEventHandler AlarmEASet;

		void ILatchDriver.SetState(int id, int value)
		{
			SendState(GetSmsById(id), value);
		}
		
		void ILatchDriver.ReceiveState(int id)
		{
			ReceiveState(GetSmsById(id));
		}
		
		void ILatchDriver.Reset(int id)
		{
			ResetLatch(GetSmsById(id));
		}
		
		#endregion
		
		#region LatchDriver methods
		void SetAutomaticMode(string smsNo)
		{
			if(AutomaticModeSet != null)
				AutomaticModeSet(this, new LatchEventArgs(GetIdBySms(smsNo), smsNo));
		}
		
		void SetLatchValue(string smsNo, int value)
		{
			if(LatchStateReceived != null)
			{
				LatchStateReceived(this, new LatchValueEventArgs(GetIdBySms(smsNo), smsNo, value));
			}		
		}
		
		void SetManualMode(string smsNo)
		{
			if(ManualModeSet != null)
				ManualModeSet(this, new LatchEventArgs(GetIdBySms(smsNo), smsNo));
		}
		
		void SetDoorOpenAlarm(string smsNo)
		{
			if(AlarmDoorOpenSet != null)
				AlarmDoorOpenSet(this, new LatchEventArgs(GetIdBySms(smsNo), smsNo));
		}
		
		void SetControlMotorAlarm(string smsNo)
		{
			if(AlarmMotorErrorSet != null)
				AlarmMotorErrorSet(this, new LatchEventArgs(GetIdBySms(smsNo), smsNo));
		}
		
		void SetEAAlarm(string smsNo)
		{
			if(AlarmEASet != null)
				AlarmEASet(this, new LatchEventArgs(GetIdBySms(smsNo), smsNo));
		}		
		#endregion

	}
}

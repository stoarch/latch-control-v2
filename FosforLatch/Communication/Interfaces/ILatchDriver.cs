﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 04.09.2018
 * Time: 10:26
 * 
 */
using System;

namespace FosforLatch.Communication.Interfaces
{
	/// <summary>
	/// Description of ILatchDriver.
	/// </summary>
	public interface ILatchDriver
	{
		event LatchValueEventHandler LatchStateReceived;	

		event LatchEventHandler AutomaticModeSet;
		event LatchEventHandler ManualModeSet;
		event LatchEventHandler AlarmDoorOpenSet;
		event LatchEventHandler AlarmMotorErrorSet;
		event LatchEventHandler AlarmEASet;

		void SetState(int id, int value);
		void ReceiveState(int id);
		void Reset(int id);
	}
}

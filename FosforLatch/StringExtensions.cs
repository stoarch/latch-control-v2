﻿/*
 * Created by SharpDevelop.
 * User: 1
 * Date: 30.04.2018
 * Time: 10:50
 * 
 */
using System;

namespace FosforLatch
{
	/// <summary>
	/// new methods for strings
	/// </summary>
	public static class StringExtensions
	{
		public static string RemoveCarriageReturn(this string source, char crFiller = ' ', char nlFiller = ' ')
		{
			return source.Replace((char)(13),crFiller).Replace((char)(10),nlFiller);	//we need one char instead of cr/lf so it will be spaced	
		}
	}
}

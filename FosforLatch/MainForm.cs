﻿/*
 * Created by SharpDevelop.
 * User: Vladimir Afonin
 * Date: 21.04.2018
 * Time: 13:34
 * 
 * Manage fosfor latch by sms 
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

using FosforLatch.Communication;
using FosforLatch.Communication.Interfaces;
using FosforLatch.GUI.Controls;
using FosforLatch.GUI.Views;
using FosforLatch.Interfaces;
using IO.Modems.Interfaces;
using Tools.Interfaces;

namespace FosforLatch
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form, ILineHandlerView, ISmsReporter, ILogger
	{
		const int FOSFOR_LATCH = 0;
		const int ALFARABI_LATCH = 1;

		const int MAX_LOG_LINES = 100;
		
		const int RECEIVE_TIMEOUT_SEC = 1;

		ReceivedLineHandler lineHandler_ = new ReceivedLineHandler();
		
		Modem modem_ = new Modem();
		ILatchDriver latchDriver_;
		
		bool portOpening_ = false; //during open sequence it is true
		event EventHandler onPortOpened_;
		
		List<LatchView> latchViews_;
		List<LatchLevelControl> latchControls_;
		
		bool smsSent_ = false;
		

		public MainForm()
		{
			InitializeComponent();
			
			InitLog();
			
			modem_.Port = serialPort;
			modem_.LineHandler = lineHandler_;
			modem_.InitPort();
			modem_.Logger = this;
			modem_.SmsReporter = this;

			lineHandler_.LineView = this;
			lineHandler_.Modem = modem_;
			
			var driver = new LatchSmsDriver();
			driver.SmsProvider = modem_;
			driver.Log = this;
			latchDriver_ = driver as ILatchDriver;		
			
			
			latchDriver_.LatchStateReceived += new LatchValueEventHandler(latchDriver__LatchStateReceived);
			latchDriver_.AlarmDoorOpenSet += new LatchEventHandler(latchDriver__AlarmDoorOpenSet);
			latchDriver_.AlarmEASet += new LatchEventHandler(latchDriver__AlarmEASet);
			latchDriver_.AlarmMotorErrorSet += new LatchEventHandler(latchDriver__AlarmMotorErrorSet);
			latchDriver_.AutomaticModeSet += new LatchEventHandler(latchDriver__AutomaticModeSet);
			latchDriver_.ManualModeSet += new LatchEventHandler(latchDriver__ManualModeSet);
						
			
			latchViews_ = new List<LatchView>();
			latchViews_.Add(fosfor_latchView);
			latchViews_.Add(alfarabi_latchView);
			
			latchControls_ = new List<LatchLevelControl>();
			latchControls_.Add(fosfor_latchLevelControl);
			latchControls_.Add(alfarabi_latchLevelControl);
			
			fosfor_latchLevelControl.ReadLevelEvent += new EventHandler(fosfor_latchLevelControl_ReadLevelEvent);
			fosfor_latchLevelControl.SetLevelEvent += new LatchLevelEventHandler(fosfor_latchLevelControl_SetLevelEvent);
			fosfor_latchLevelControl.ResetEvent += new EventHandler(fosfor_latchLevelControl_ResetEvent);
			
			alfarabi_latchLevelControl.ReadLevelEvent += new EventHandler(alfarabi_latchLevelControl_ReadLevelEvent);
			alfarabi_latchLevelControl.SetLevelEvent += new LatchLevelEventHandler(alfarabi_latchLevelControl_SetLevelEvent);
			alfarabi_latchLevelControl.ResetEvent += new EventHandler(alfarabi_latchLevelControl_ResetEvent);
			
			onPortOpened_ += new EventHandler(HandlePortOpened);
		}

		void alfarabi_latchLevelControl_ResetEvent(object sender, EventArgs e)
		{
			LogText("Alfarabi latch reset event");
			ResetLatch(ALFARABI_LATCH);
		}

		void fosfor_latchLevelControl_ResetEvent(object sender, EventArgs e)
		{
			LogText("Fosfor latch reset event");
			ResetLatch(FOSFOR_LATCH);
		}
		
		const string LOG_PATH = ".\\Log\\";
		const string LOG_FILE_TEMPLATE = "latch{0}.log";
		string logFileName;
		StreamWriter logStream;
		
		void InitLog()
		{
			if(!File.Exists(LOG_PATH))
			{
				Directory.CreateDirectory(LOG_PATH);
			}
			
			logFileName = LOG_PATH + String.Format(LOG_FILE_TEMPLATE,  DateTime.Now.ToString("%y%M%d-%h%m%s"));
			
			using( var writer = File.CreateText(logFileName))
			{
				writer.WriteLine("----LOG STARTED----");
				writer.WriteLine("{0} {1}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString());
			}
		}
		
		void WriteToLogFile(string message)
		{
			using(var writer = File.AppendText(logFileName))
			{
				writer.WriteLine(message);
			}
		}

		void alfarabi_latchLevelControl_SetLevelEvent(object sender, LatchLevelEventArgs args)
		{
			LogText("Alfarabi latch set level event with: " + args.Value);
			
			SetLatchValue(ALFARABI_LATCH, args.Value);
		}
		
		void SetLatchValue(int latchId, int value )
		{
			SetMainStatus("Обработка");
			SetWorkStatus("Устанавливаю уровень");
			SetHelpStatus("Подождите, пожалуйста...");

			RestartSmsSentWatchdogTimer();
			RestartSmsTimeoutWatchdogTimer();

			DisableControlUI();

			latchDriver_.SetState(latchId, value);
		}

		void ResetLatch(int latchId)
		{
			SetMainStatus("Обработка");
			SetWorkStatus("Сбрасываю контроллер");
			SetHelpStatus("Подождите, пожалуйста...");

			RestartSmsSentWatchdogTimer();
			RestartSmsTimeoutWatchdogTimer();

			DisableControlUI();

			latchDriver_.Reset(latchId);
		}
		
		void DisableControlUI()
		{
			foreach (var control in latchControls_) {
				control.Enabled = false;
			}
		}

		void alfarabi_latchLevelControl_ReadLevelEvent(object sender, EventArgs e)
		{
			LogText("Alfarabi latch read level event");
			ReadLatchLevel(ALFARABI_LATCH);
		}
		
		void ReadLatchLevel(int latchId)
		{
			SetMainStatus("Обработка");
			SetWorkStatus("Читаю уровень");
			SetHelpStatus("Подождите, пожалуйста...");
			
			RestartSmsSentWatchdogTimer();
			RestartSmsTimeoutWatchdogTimer();

			DisableControlUI();

			latchDriver_.ReceiveState(latchId);
		}
		
		void SetMainStatus(string message)
		{
			mainStatus_toolStripStatusLabel.Text = message;
		}

		void SetWorkStatus(string message)
		{
			workStatus_toolStripStatusLabel.Text = message;
		}
		
		void SetHelpStatus(string message)
		{
			help_toolStripStatusLabel.Text = message;
		}
		
		void fosfor_latchLevelControl_SetLevelEvent(object sender, LatchLevelEventArgs args)
		{
			LogText("Fosfor latch set level event with " + args.Value);
			SetLatchValue(FOSFOR_LATCH, args.Value);
		}

		
		void fosfor_latchLevelControl_ReadLevelEvent(object sender, EventArgs e)
		{
			LogText("Fosfor latch read level event");
			ReadLatchLevel(FOSFOR_LATCH);
		}
		
		ILatchView GetLatchView(int id)
		{			
			return latchViews_[id] as ILatchView;
		}

		void latchDriver__ManualModeSet(object sender, LatchEventArgs args)
		{
			LogText("Manual mode set:" + args);

			SetDefaultStatus();
			EnableControlUI();
			
			GetLatchView(args.Id).SetManualMode();
		}

		void latchDriver__AutomaticModeSet(object sender, LatchEventArgs args)
		{
			LogText("Automatic mode set: " + args);

			SetDefaultStatus();
			EnableControlUI();
			
			GetLatchView(args.Id).SetAutomaticMode();
		}

		void latchDriver__AlarmMotorErrorSet(object sender, LatchEventArgs args)
		{
			LogText("Alarm motor:" + args);

			SetDefaultStatus();
			EnableControlUI();
			
			GetLatchView(args.Id).SetControlMotorAlarm();
		}

		void latchDriver__AlarmEASet(object sender, LatchEventArgs args)
		{
			LogText("Alarm EA set:" + args);

			SetDefaultStatus();
			EnableControlUI();
			
			GetLatchView(args.Id).SetEAAlarm();
		}

		void latchDriver__AlarmDoorOpenSet(object sender, LatchEventArgs args)
		{
			LogText("Latch alarm door set: " + args);

			SetDefaultStatus();
			EnableControlUI();
			
			GetLatchView(args.Id).SetDoorOpenAlarm();
		}

		void latchDriver__LatchStateReceived(object sender, LatchValueEventArgs args)
		{
			LogText("Latch state received: " + args.ToString());
			
			SetDefaultStatus();
			EnableControlUI();

			GetLatchView(args.Id).SetLatchValue(args.Value);
		}

		void SetDefaultStatus()
		{
			SetMainStatus("Готово");
			SetWorkStatus("Ожидаю команды");
			SetHelpStatus("Выберите команду установки или чтения");
		}
		
		void HandlePortOpened(object sender, EventArgs e)
		{
			RemoveAllSms();
			//ReadLatchState();
		}
		
		
				
		
		delegate void HandlerCallback();
		
		public void SetSmsSentCheckBox()
		{
			if(smsSent_checkBox.InvokeRequired)
			{
				HandlerCallback d = SetSmsSentCheckBox;
				
				Invoke(d);
				return;
			}		

			smsSent_checkBox.Checked = true;
			smsSentWatchdog_timer.Enabled = false;
		}

		public void SetSmsHandledCheckBox()
		{
			if(smsHandled_checkBox.InvokeRequired)
			{
				HandlerCallback d = SetSmsHandledCheckBox;
				
				Invoke(d);
				return;
			}
			smsHandled_checkBox.Checked = true;
		}
		
		public void SetSmsReceivedCheckBox()
		{
			if(smsReceived_checkBox.InvokeRequired)
			{
				HandlerCallback d = SetSmsReceivedCheckBox;
				
				Invoke(d);
				return;
			}
			smsReceived_checkBox.Checked = true;
			smsReceivedTimeoutWatchdog_timer.Enabled = false;
		}
		
		
		public void ClearSmsStatusUI()
		{			
			ClearSmsHandledCheckBox();
			ClearSmsReceviedCheckBox();	
			ClearSmsSentCheckBox();
		}
		
		void ClearSmsHandledCheckBox()
		{
			if(smsHandled_checkBox.InvokeRequired)
			{
				HandlerCallback d = ClearSmsHandledCheckBox;
				Invoke(d);
				return;
			}
			
			smsHandled_checkBox.Checked = false;
		}
		
		
		
		void ClearSmsReceviedCheckBox()
		{
			if(smsReceived_checkBox.InvokeRequired)
			{
				HandlerCallback d = ClearSmsReceviedCheckBox;
				Invoke(d);
				return;
			}
			smsReceived_checkBox.Checked = false;			
		}
		
		void ClearSmsSentCheckBox()
		{
			if(smsSent_checkBox.InvokeRequired)
			{
				HandlerCallback d = ClearSmsSentCheckBox;
				Invoke(d);
				return;
			}
			
			smsSent_checkBox.Checked = false;
		}		
			
		void ButtonReadClick(object sender, EventArgs e)
		{
			modem_.ReadAllSms();
		}		
		
		delegate void SetTextCallback(string text);
		
		public void LogText(string msg)
		{
			if(log_textBox.InvokeRequired)
			{
				SetTextCallback d = new MainForm.SetTextCallback(LogText);
				Invoke(d, new object[]{msg});
			}
			else
			{
				string newMsg = String.Format("{0}: {1}\n", DateTime.Now.ToLongTimeString(), msg);
				
				WriteToLogFile(newMsg);
				WriteToEventLog(newMsg);

				if(log_textBox.Lines.Length > MAX_LOG_LINES)
				{
					var lines = log_textBox.Lines;
					log_textBox.Lines = lines.Skip(1).ToArray();
				}

				log_textBox.AppendText(newMsg);				
			}
		}
		
		void WriteToEventLog(string msg)
		{
			using(var appLog = new EventLog("Application"))
			{
				appLog.Source = "LatchControl";
				appLog.WriteEntry(msg);
			}
			      
		}
		
		
		void ButtonOpenPortClick(object sender, EventArgs e)
		{
			OpenPort();
		}
		
		void OpenPort()
		{
			if(!serialPort.IsOpen)
			{
				portOpening_ = true;
				serialPort.Open();
			}
			
			buttonOpenPort.Enabled = false;			
		}
		
		
		void StatusTimerTick(object sender, EventArgs e)
		{
			portOpened_checkBox.Checked = serialPort.IsOpen;
			
			if(portOpening_ && serialPort.IsOpen)
			{
				portOpening_ = false; //now it open
				FireOnPortOpened();
			}
			
			buttonOpenPort.Enabled = !portOpened_checkBox.Checked;
			buttonClosePort.Enabled = portOpened_checkBox.Checked;
			
			if(smsSent_)
			{
				showSpinningProgressStep();
				showSmsRemainingTime();
			}
			else
			{
				hideSpinningProgress();
				hideSmsRemainingTime();
			}
		}
		
		int smsWaitTick_ = 0;
		
		void showSmsRemainingTime()
		{
			int sec = smsSentWatchdog_timer.Interval/1000 - smsWaitTick_;
			
			int min = sec / 60;
			sec = sec - min*60;
			
			smsWaitTick_ += 1;
			
			waitTime_toolStripStatusLabel.Text = String.Format("{0}:{1}", min, sec);
		}
		
		void hideSmsRemainingTime()
		{
			smsWaitTick_ = 0;
			waitTime_toolStripStatusLabel.Text = "0:00";
		}
		
		int curStep_ = 0;
		string[] progressSteps_ = new string[]{"\\","|","/","-","\\","|","-"};
				
		void showSpinningProgressStep()
		{
			string curProgress = progressSteps_[curStep_++ % progressSteps_.Length];
			
			progress_toolStripStatusLabel.Text = curProgress;			
		}
		
		void hideSpinningProgress()
		{
			progress_toolStripStatusLabel.Text = "*";
		}
		
		void FireOnPortOpened()
		{
			if(onPortOpened_ != null)
			{
				onPortOpened_(this, new EventArgs());
			}
		}
		
		void ButtonCloseClick(object sender, EventArgs e)
		{
			ClosePort();
		}
		
		void ClosePort()
		{
			serialPort.Close();
			buttonClosePort.Enabled = false;
		}
		
		void MainFormFormClosed(object sender, FormClosedEventArgs e)
		{
			ClosePort();
		}
		
		void MainFormLoad(object sender, EventArgs e)
		{			
			OpenPort();
		}
		
		void ButtonReadStateClick(object sender, EventArgs e)
		{
			ReadLatchState();
		}
		
		void ReadLatchState()
		{
			RestartSmsSentWatchdogTimer();			
			RestartSmsTimeoutWatchdogTimer();
			
			modem_.SendSms("state");
		}		
		
		void RestartSmsSentWatchdogTimer()
		{
			smsSentWatchdog_timer.Enabled = false;//to restart timer
			smsSentWatchdog_timer.Enabled = true;		
		}
		
		void RestartSmsTimeoutWatchdogTimer()
		{
			smsReceivedTimeoutWatchdog_timer.Enabled = false;
			smsReceivedTimeoutWatchdog_timer.Enabled = true;
		}
		
		void RemoveAllSms_buttonClick(object sender, EventArgs e)
		{
			RemoveAllSms();			
		}
		
		void RemoveAllSms()
		{
			modem_.RemoveAllSms();
		}
		
		void ReadSms_buttonClick(object sender, EventArgs e)
		{
			int smsNo = Convert.ToInt16(smsNo_textBox.Text);
			ReadSmsAt(smsNo);
		}
		
		void ReadSmsAt(int smsNo)
		{
			modem_.ReadSmsAt(smsNo);
		}
				
		void SendNewLatchValue_buttonClick(object sender, EventArgs e)
		{
			//TODO: Remove this
		}
		
		
		
		void ButtonClearLogClick(object sender, EventArgs e)
		{
			log_textBox.Clear();
		}
		
		void ButtonSendEscClick(object sender, EventArgs e)
		{
			modem_.SendToSerial("" + (char)(27));
		}
		
		void TimerWatchdogTick(object sender, EventArgs e)
		{
			if(modem_.IsNewSmsState())
			{
				modem_.ResetSmsState();
			}

			LogText("WARNING: Watchdog:: Modem sending sms state reset!");
			smsSentWatchdog_timer.Enabled = false;
			EnableControlUI();
			
			SetMainStatus("Готово");
			SetWorkStatus("Получена ошибка");
			SetHelpStatus("Модем завис на получении смс от задвижки");
			
			smsSent_ = false;
		}
		
		void SmsReceivedTimeoutWatchdog_timerTick(object sender, EventArgs e)
		{
			LogText("ERROR:: Sms receiving timeout!");
			modem_.ResetSmsState();
			smsReceivedTimeoutWatchdog_timer.Enabled = false;
			EnableControlUI();

			SetMainStatus("Готово");
			SetWorkStatus("Получена ошибка");
			SetHelpStatus("Модем не смог получить смс от задвижки");
			
			smsSent_ = false;
		}
		
		void EnableControlUI()
		{
			if(InvokeRequired){
				HandlerCallback d = EnableControlUI;
				
				Invoke(d);
				return;
			}
			
			foreach (var control in latchControls_) 
			{
				control.Enabled = true;
			}
		}
		
		void Latch_trackBarScroll(object sender, EventArgs e)
		{
		}		
		
		void Valve_pictureBoxClick(object sender, EventArgs e)
		{
			
		}		
		
		void ISmsReporter.SetSmsReceived()
		{
			smsSent_ = false;
			
			LogText("Sms received");
			SetSmsReceivedCheckBox();
		}
		
		void ISmsReporter.SetSmsSent()
		{
			smsSent_ = true;
			
			LogText("Sms sent");
			SetSmsSentCheckBox();
		}
		
		void ISmsReporter.SetSmsHandled()
		{
			LogText("Sms handled");
			SetSmsHandledCheckBox();
			EnableControlUI();
		}
		
		void ISmsReporter.ClearSmsStatus()
		{
			LogText("Sms status cleared");
			ClearSmsStatusUI();
		}		

		// ILineHandlerView //
		
		void ILineHandlerView.SetReceivedOk()
		{
			LogText("Ok received");
		}
		
		void ILineHandlerView.SetReceivedError()
		{
			LogText("Error received");
		}
		
		public void ParseSmsHeader(List<string> smsContents)
		{
			throw new NotImplementedException();
		}
		
		void ISmsReporter.SetNoMoreSms()
		{
			LogText("No more sms");
		}
	}
}

﻿/*
 * Created by SharpDevelop.
 * User: 1
 * Date: 21.04.2018
 * Time: 13:34
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace FosforLatch
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.buttonReadSms = new System.Windows.Forms.Button();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.mainStatus_toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.workStatus_toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.help_toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.progress_toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.waitTime_toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.label13 = new System.Windows.Forms.Label();
			this.log_textBox = new System.Windows.Forms.TextBox();
			this.serialPort = new System.IO.Ports.SerialPort(this.components);
			this.portOpened_checkBox = new System.Windows.Forms.CheckBox();
			this.buttonOpenPort = new System.Windows.Forms.Button();
			this.statusTimer = new System.Windows.Forms.Timer(this.components);
			this.buttonClosePort = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.smsHandled_checkBox = new System.Windows.Forms.CheckBox();
			this.smsReceived_checkBox = new System.Windows.Forms.CheckBox();
			this.smsSent_checkBox = new System.Windows.Forms.CheckBox();
			this.removeAllSms_button = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.buttonSendEsc = new System.Windows.Forms.Button();
			this.buttonClearLog = new System.Windows.Forms.Button();
			this.smsNo_textBox = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.readSms_button = new System.Windows.Forms.Button();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.smsSentWatchdog_timer = new System.Windows.Forms.Timer(this.components);
			this.smsReceivedTimeoutWatchdog_timer = new System.Windows.Forms.Timer(this.components);
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.scheme_tabPage = new System.Windows.Forms.TabPage();
			this.alfarabi_latchLevelControl = new FosforLatch.GUI.Controls.LatchLevelControl();
			this.fosfor_latchLevelControl = new FosforLatch.GUI.Controls.LatchLevelControl();
			this.alfarabi_latchView = new FosforLatch.GUI.Views.LatchView();
			this.fosfor_latchView = new FosforLatch.GUI.Views.LatchView();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.statusStrip1.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.scheme_tabPage.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.SuspendLayout();
			// 
			// buttonReadSms
			// 
			this.buttonReadSms.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonReadSms.Location = new System.Drawing.Point(887, 79);
			this.buttonReadSms.Name = "buttonReadSms";
			this.buttonReadSms.Size = new System.Drawing.Size(95, 41);
			this.buttonReadSms.TabIndex = 3;
			this.buttonReadSms.Text = "Прочесть ВСЕ SMS";
			this.buttonReadSms.UseVisualStyleBackColor = true;
			this.buttonReadSms.Click += new System.EventHandler(this.ButtonReadClick);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.mainStatus_toolStripStatusLabel,
			this.workStatus_toolStripStatusLabel,
			this.help_toolStripStatusLabel,
			this.progress_toolStripStatusLabel,
			this.waitTime_toolStripStatusLabel});
			this.statusStrip1.Location = new System.Drawing.Point(0, 627);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(1018, 22);
			this.statusStrip1.TabIndex = 4;
			this.statusStrip1.Text = "statusStrip";
			// 
			// mainStatus_toolStripStatusLabel
			// 
			this.mainStatus_toolStripStatusLabel.Name = "mainStatus_toolStripStatusLabel";
			this.mainStatus_toolStripStatusLabel.Size = new System.Drawing.Size(45, 17);
			this.mainStatus_toolStripStatusLabel.Text = "Готово";
			// 
			// workStatus_toolStripStatusLabel
			// 
			this.workStatus_toolStripStatusLabel.Name = "workStatus_toolStripStatusLabel";
			this.workStatus_toolStripStatusLabel.Size = new System.Drawing.Size(107, 17);
			this.workStatus_toolStripStatusLabel.Text = "Ожидаю команды";
			// 
			// help_toolStripStatusLabel
			// 
			this.help_toolStripStatusLabel.Name = "help_toolStripStatusLabel";
			this.help_toolStripStatusLabel.Size = new System.Drawing.Size(811, 17);
			this.help_toolStripStatusLabel.Spring = true;
			this.help_toolStripStatusLabel.Text = "Выберите требуемый уровень и нажмите \"Установить\" (и подождите 10-20 секунд)";
			// 
			// progress_toolStripStatusLabel
			// 
			this.progress_toolStripStatusLabel.Name = "progress_toolStripStatusLabel";
			this.progress_toolStripStatusLabel.Size = new System.Drawing.Size(12, 17);
			this.progress_toolStripStatusLabel.Text = "*";
			// 
			// waitTime_toolStripStatusLabel
			// 
			this.waitTime_toolStripStatusLabel.Name = "waitTime_toolStripStatusLabel";
			this.waitTime_toolStripStatusLabel.Size = new System.Drawing.Size(28, 17);
			this.waitTime_toolStripStatusLabel.Text = "0:00";
			// 
			// label13
			// 
			this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label13.Location = new System.Drawing.Point(8, 263);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(100, 23);
			this.label13.TabIndex = 18;
			this.label13.Text = "Протокол:";
			// 
			// log_textBox
			// 
			this.log_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
			| System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.log_textBox.Location = new System.Drawing.Point(17, 21);
			this.log_textBox.Multiline = true;
			this.log_textBox.Name = "log_textBox";
			this.log_textBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.log_textBox.Size = new System.Drawing.Size(731, 565);
			this.log_textBox.TabIndex = 20;
			// 
			// serialPort
			// 
			this.serialPort.PortName = "COM3";
			// 
			// portOpened_checkBox
			// 
			this.portOpened_checkBox.AutoCheck = false;
			this.portOpened_checkBox.Location = new System.Drawing.Point(15, 18);
			this.portOpened_checkBox.Name = "portOpened_checkBox";
			this.portOpened_checkBox.Size = new System.Drawing.Size(73, 24);
			this.portOpened_checkBox.TabIndex = 22;
			this.portOpened_checkBox.Text = "Открыт";
			this.portOpened_checkBox.UseVisualStyleBackColor = true;
			// 
			// buttonOpenPort
			// 
			this.buttonOpenPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonOpenPort.Location = new System.Drawing.Point(887, 21);
			this.buttonOpenPort.Name = "buttonOpenPort";
			this.buttonOpenPort.Size = new System.Drawing.Size(95, 23);
			this.buttonOpenPort.TabIndex = 23;
			this.buttonOpenPort.Text = "Открыть";
			this.buttonOpenPort.UseVisualStyleBackColor = true;
			this.buttonOpenPort.Click += new System.EventHandler(this.ButtonOpenPortClick);
			// 
			// statusTimer
			// 
			this.statusTimer.Enabled = true;
			this.statusTimer.Interval = 1000;
			this.statusTimer.Tick += new System.EventHandler(this.StatusTimerTick);
			// 
			// buttonClosePort
			// 
			this.buttonClosePort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonClosePort.Enabled = false;
			this.buttonClosePort.Location = new System.Drawing.Point(887, 50);
			this.buttonClosePort.Name = "buttonClosePort";
			this.buttonClosePort.Size = new System.Drawing.Size(95, 23);
			this.buttonClosePort.TabIndex = 24;
			this.buttonClosePort.Text = "Закрыть";
			this.buttonClosePort.UseVisualStyleBackColor = true;
			this.buttonClosePort.Click += new System.EventHandler(this.ButtonCloseClick);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.smsHandled_checkBox);
			this.groupBox1.Controls.Add(this.smsReceived_checkBox);
			this.groupBox1.Controls.Add(this.smsSent_checkBox);
			this.groupBox1.Location = new System.Drawing.Point(754, 13);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(127, 113);
			this.groupBox1.TabIndex = 30;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Sms";
			// 
			// smsHandled_checkBox
			// 
			this.smsHandled_checkBox.AutoCheck = false;
			this.smsHandled_checkBox.Location = new System.Drawing.Point(11, 83);
			this.smsHandled_checkBox.Name = "smsHandled_checkBox";
			this.smsHandled_checkBox.Size = new System.Drawing.Size(104, 24);
			this.smsHandled_checkBox.TabIndex = 32;
			this.smsHandled_checkBox.Text = "Обработано";
			this.smsHandled_checkBox.UseVisualStyleBackColor = true;
			// 
			// smsReceived_checkBox
			// 
			this.smsReceived_checkBox.AutoCheck = false;
			this.smsReceived_checkBox.Location = new System.Drawing.Point(11, 55);
			this.smsReceived_checkBox.Name = "smsReceived_checkBox";
			this.smsReceived_checkBox.Size = new System.Drawing.Size(104, 24);
			this.smsReceived_checkBox.TabIndex = 31;
			this.smsReceived_checkBox.Text = "Получено";
			this.smsReceived_checkBox.UseVisualStyleBackColor = true;
			// 
			// smsSent_checkBox
			// 
			this.smsSent_checkBox.AutoCheck = false;
			this.smsSent_checkBox.Location = new System.Drawing.Point(11, 25);
			this.smsSent_checkBox.Name = "smsSent_checkBox";
			this.smsSent_checkBox.Size = new System.Drawing.Size(104, 24);
			this.smsSent_checkBox.TabIndex = 30;
			this.smsSent_checkBox.Text = "Отправлено";
			this.smsSent_checkBox.UseVisualStyleBackColor = true;
			// 
			// removeAllSms_button
			// 
			this.removeAllSms_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.removeAllSms_button.Location = new System.Drawing.Point(887, 126);
			this.removeAllSms_button.Name = "removeAllSms_button";
			this.removeAllSms_button.Size = new System.Drawing.Size(95, 35);
			this.removeAllSms_button.TabIndex = 31;
			this.removeAllSms_button.Text = "Удалить ВСЕ SMS";
			this.removeAllSms_button.UseVisualStyleBackColor = true;
			this.removeAllSms_button.Click += new System.EventHandler(this.RemoveAllSms_buttonClick);
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
			| System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.buttonSendEsc);
			this.groupBox2.Controls.Add(this.buttonClearLog);
			this.groupBox2.Controls.Add(this.smsNo_textBox);
			this.groupBox2.Controls.Add(this.label16);
			this.groupBox2.Controls.Add(this.readSms_button);
			this.groupBox2.Controls.Add(this.groupBox3);
			this.groupBox2.Controls.Add(this.log_textBox);
			this.groupBox2.Controls.Add(this.groupBox1);
			this.groupBox2.Controls.Add(this.removeAllSms_button);
			this.groupBox2.Controls.Add(this.label13);
			this.groupBox2.Controls.Add(this.buttonReadSms);
			this.groupBox2.Controls.Add(this.buttonClosePort);
			this.groupBox2.Controls.Add(this.buttonOpenPort);
			this.groupBox2.Location = new System.Drawing.Point(6, 6);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(998, 592);
			this.groupBox2.TabIndex = 32;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Администрирование";
			// 
			// buttonSendEsc
			// 
			this.buttonSendEsc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonSendEsc.Location = new System.Drawing.Point(887, 290);
			this.buttonSendEsc.Name = "buttonSendEsc";
			this.buttonSendEsc.Size = new System.Drawing.Size(95, 23);
			this.buttonSendEsc.TabIndex = 37;
			this.buttonSendEsc.Text = "Послать ESC";
			this.buttonSendEsc.UseVisualStyleBackColor = true;
			this.buttonSendEsc.Click += new System.EventHandler(this.ButtonSendEscClick);
			// 
			// buttonClearLog
			// 
			this.buttonClearLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonClearLog.Location = new System.Drawing.Point(887, 245);
			this.buttonClearLog.Name = "buttonClearLog";
			this.buttonClearLog.Size = new System.Drawing.Size(95, 39);
			this.buttonClearLog.TabIndex = 36;
			this.buttonClearLog.Text = "Очистить протокол";
			this.buttonClearLog.UseVisualStyleBackColor = true;
			this.buttonClearLog.Click += new System.EventHandler(this.ButtonClearLogClick);
			// 
			// smsNo_textBox
			// 
			this.smsNo_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.smsNo_textBox.Location = new System.Drawing.Point(887, 190);
			this.smsNo_textBox.Name = "smsNo_textBox";
			this.smsNo_textBox.Size = new System.Drawing.Size(95, 20);
			this.smsNo_textBox.TabIndex = 35;
			this.smsNo_textBox.Text = "1";
			this.smsNo_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label16
			// 
			this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label16.Location = new System.Drawing.Point(887, 170);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(100, 20);
			this.label16.TabIndex = 34;
			this.label16.Text = "SMS no:";
			// 
			// readSms_button
			// 
			this.readSms_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.readSms_button.Location = new System.Drawing.Point(887, 216);
			this.readSms_button.Name = "readSms_button";
			this.readSms_button.Size = new System.Drawing.Size(95, 23);
			this.readSms_button.TabIndex = 33;
			this.readSms_button.Text = "Прочесть SMS";
			this.readSms_button.UseVisualStyleBackColor = true;
			this.readSms_button.Click += new System.EventHandler(this.ReadSms_buttonClick);
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this.portOpened_checkBox);
			this.groupBox3.Location = new System.Drawing.Point(754, 139);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(127, 51);
			this.groupBox3.TabIndex = 32;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Порт:";
			// 
			// smsSentWatchdog_timer
			// 
			this.smsSentWatchdog_timer.Enabled = true;
			this.smsSentWatchdog_timer.Interval = 120000;
			this.smsSentWatchdog_timer.Tick += new System.EventHandler(this.TimerWatchdogTick);
			// 
			// smsReceivedTimeoutWatchdog_timer
			// 
			this.smsReceivedTimeoutWatchdog_timer.Interval = 120000;
			this.smsReceivedTimeoutWatchdog_timer.Tick += new System.EventHandler(this.SmsReceivedTimeoutWatchdog_timerTick);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.scheme_tabPage);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(1018, 627);
			this.tabControl1.TabIndex = 41;
			// 
			// scheme_tabPage
			// 
			this.scheme_tabPage.BackColor = System.Drawing.Color.Gainsboro;
			this.scheme_tabPage.Controls.Add(this.alfarabi_latchLevelControl);
			this.scheme_tabPage.Controls.Add(this.fosfor_latchLevelControl);
			this.scheme_tabPage.Controls.Add(this.alfarabi_latchView);
			this.scheme_tabPage.Controls.Add(this.fosfor_latchView);
			this.scheme_tabPage.Location = new System.Drawing.Point(4, 22);
			this.scheme_tabPage.Name = "scheme_tabPage";
			this.scheme_tabPage.Padding = new System.Windows.Forms.Padding(3);
			this.scheme_tabPage.Size = new System.Drawing.Size(1010, 601);
			this.scheme_tabPage.TabIndex = 0;
			this.scheme_tabPage.Text = "Схема";
			// 
			// alfarabi_latchLevelControl
			// 
			this.alfarabi_latchLevelControl.Location = new System.Drawing.Point(533, 421);
			this.alfarabi_latchLevelControl.Max = 80;
			this.alfarabi_latchLevelControl.Min = 10;
			this.alfarabi_latchLevelControl.Name = "alfarabi_latchLevelControl";
			this.alfarabi_latchLevelControl.Size = new System.Drawing.Size(445, 158);
			this.alfarabi_latchLevelControl.TabIndex = 4;
			// 
			// fosfor_latchLevelControl
			// 
			this.fosfor_latchLevelControl.Location = new System.Drawing.Point(33, 421);
			this.fosfor_latchLevelControl.Max = 100;
			this.fosfor_latchLevelControl.Min = 0;
			this.fosfor_latchLevelControl.Name = "fosfor_latchLevelControl";
			this.fosfor_latchLevelControl.Size = new System.Drawing.Size(445, 158);
			this.fosfor_latchLevelControl.TabIndex = 3;
			// 
			// alfarabi_latchView
			// 
			this.alfarabi_latchView.BackColor = System.Drawing.SystemColors.Info;
			this.alfarabi_latchView.Caption = "Задвижка: Алатау (Кунаева)";
			this.alfarabi_latchView.Location = new System.Drawing.Point(533, 21);
			this.alfarabi_latchView.Name = "alfarabi_latchView";
			this.alfarabi_latchView.Size = new System.Drawing.Size(445, 394);
			this.alfarabi_latchView.TabIndex = 2;
			// 
			// fosfor_latchView
			// 
			this.fosfor_latchView.BackColor = System.Drawing.SystemColors.Info;
			this.fosfor_latchView.Caption = "Задвижка: Фосфор";
			this.fosfor_latchView.Location = new System.Drawing.Point(33, 21);
			this.fosfor_latchView.Name = "fosfor_latchView";
			this.fosfor_latchView.Size = new System.Drawing.Size(445, 394);
			this.fosfor_latchView.TabIndex = 1;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.groupBox2);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(1010, 601);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Администрирование";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1018, 649);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.statusStrip1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.Text = "Задвижка МСЧ Фосфор";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainFormFormClosed);
			this.Load += new System.EventHandler(this.MainFormLoad);
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.scheme_tabPage.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.ToolStripStatusLabel waitTime_toolStripStatusLabel;
		private FosforLatch.GUI.Controls.LatchLevelControl alfarabi_latchLevelControl;
		private FosforLatch.GUI.Controls.LatchLevelControl fosfor_latchLevelControl;
		private FosforLatch.GUI.Views.LatchView fosfor_latchView;
		private FosforLatch.GUI.Views.LatchView alfarabi_latchView;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage scheme_tabPage;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.ToolStripStatusLabel progress_toolStripStatusLabel;
		private System.Windows.Forms.Timer smsReceivedTimeoutWatchdog_timer;
		private System.Windows.Forms.Timer smsSentWatchdog_timer;
		private System.Windows.Forms.Button buttonSendEsc;
		private System.Windows.Forms.Button buttonClearLog;
		private System.Windows.Forms.Button readSms_button;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox smsNo_textBox;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button removeAllSms_button;
		private System.Windows.Forms.CheckBox smsSent_checkBox;
		private System.Windows.Forms.CheckBox smsReceived_checkBox;
		private System.Windows.Forms.CheckBox smsHandled_checkBox;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button buttonClosePort;
		private System.Windows.Forms.Timer statusTimer;
		private System.Windows.Forms.Button buttonOpenPort;
		private System.Windows.Forms.CheckBox portOpened_checkBox;
		private System.IO.Ports.SerialPort serialPort;
		private System.Windows.Forms.TextBox log_textBox;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.ToolStripStatusLabel help_toolStripStatusLabel;
		private System.Windows.Forms.ToolStripStatusLabel workStatus_toolStripStatusLabel;
		private System.Windows.Forms.ToolStripStatusLabel mainStatus_toolStripStatusLabel;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.Button buttonReadSms;
	}
}

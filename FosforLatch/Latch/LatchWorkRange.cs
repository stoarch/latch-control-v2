﻿/*
 * Created by SharpDevelop.
 * User: 1
 * Date: 10.10.2018
 * Time: 12:21
 */
using System;

namespace FosforLatch.Latch
{
	/// <summary>
	/// Store and work with range of valid values for latch
	/// </summary>
	public class LatchWorkRange
	{
		public LatchWorkRange()
		{
		}
		
		public int Min { get; set; }
		public int Max { get; set; }
	}
}

﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 30.04.2018
 * Time: 10:37
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Threading;

using FosforLatch.Interfaces;

namespace FosforLatch
{
	/// <summary>
	/// Send and receive/parse modem commands and responses (AT/SMS)
	/// </summary>
	public class Modem
	{
		const string NEW_SMS_RESPONSE = ">";
		
		const string SMS_SENT_RESPONSE = "CMGS:";
		const string SMS_RECEIVED_RESPONSE = "CMTI:";
		const string SMS_READEN_RESPONSE = "CMGR:";

		const string OK_RESPONSE = "OK";
		const string ERROR_RESPONSE = "ERROR";
		
		const int MAX_SMS = 18;

		private bool needToParseSmsContents_ = false;
		private bool dataReceived_ = false;

		List<string> smsContents_ = new List<string>();

		List<string> atCommands_ = new List<string>();
		int curCommand = -1;
		
		List<string> smsCommands_ = new List<string>();
		int curSmsCommand = -1;

		string fullReceivedLine_ = "";

		string receivedLine_;
		DateTime sendTime_;
		bool needToRemoveAllSms_ = false; //set this flag if next read should delete all sms
		
		enum SmsState
		{
			Unknown,
			NewSms,
			SmsSent,
			SmsReceived,
			SmsReaden,
			SmsHandled
		}
		
		private SmsState smsState_ = SmsState.Unknown; 

		public Modem()
		{
		}
		
		public SerialPort Port { get; set; }
		
		public string ReceivedLine 
		{ 
			get
			{ 
				return receivedLine_; 
			}
			set
			{
				receivedLine_ = value;
			}
		}
		
		public string FullReceivedLine
		{
			get
			{
				return fullReceivedLine_;
			}
			set
			{
				fullReceivedLine_ = value;
			}
		}
		
		public DateTime SendTime { get{ return sendTime_; } }
		public MainForm View { get; set; }
		public IModemView ModemView {get;set;}
		public ReceivedLineHandler LineHandler { get; set; }
		
		public void SendToSerial(string msg)
		{
			if(Port == null)			
			{
				Debug.Print("Port is undefined");
				return;	
			}			
			
			if(!Port.IsOpen)
			{
				LogText("Serial port closed");
				return;
			}
			
			sendTime_ = DateTime.Now;
			receivedLine_ = "";
			
			dataReceived_ = false;
			Port.Write(msg);
			Debug.WriteLine("Written " + msg );
		}
		
		void LogText(string message)
		{
			Console.WriteLine(message);
			if(View != null)
				View.LogText(message);
			
			if(ModemView != null)
				ModemView.LogText(message);
		}
		
		void SetSmsSent()
		{
			if(View != null)
				View.SetSmsSentCheckBox();
			if(ModemView != null)
				ModemView.SetSmsSent();
		}
		
		void SetSmsReceived()
		{
			if(View != null)
				View.SetSmsReceivedCheckBox();
			if(ModemView != null)
				ModemView.SetSmsReceived();		
		}

		public void HandleReceived(string msg)
		{
			ReceivedLine += msg;
			
			if(ReceivedLine.Contains(Environment.NewLine))
			{
				LogText(ReceivedLine);
				
				if( needToParseSmsContents_ )
				{
					smsContents_.Add(ReceivedLine);
				}
								
				HandleReceivedLine();
				
				fullReceivedLine_ += ReceivedLine.RemoveCarriageReturn('|');
				ReceivedLine = "";

				LogText("Full received: " + fullReceivedLine_);
				
				
				//sms can arrive at any moment - handle it
				if(fullReceivedLine_.Contains(SMS_RECEIVED_RESPONSE))
				{
					SetSmsReceived();
					smsState_ = SmsState.SmsReceived;			

					HandleReceivedSms();
				}						
				
				if(fullReceivedLine_.Contains(SMS_READEN_RESPONSE))
				{
					ParseSMSReadenResponse();
					
					if(needToRemoveAllSms_)//clear sms buffer (modem has 20 sms only)
					{
						RemoveAllSms();
						needToRemoveAllSms_ = false;
					}
				}
			}
			
			switch (smsState_) {
				case Modem.SmsState.Unknown:
					if(fullReceivedLine_.Contains(NEW_SMS_RESPONSE)||receivedLine_.Contains(NEW_SMS_RESPONSE))
					{
						LogText("SMS START");
												
						SendNextSmsCommand();
						smsState_ = SmsState.NewSms;
					}					
					break;
					
				case Modem.SmsState.NewSms:
					if(fullReceivedLine_.Contains(SMS_SENT_RESPONSE))
					{
						SetSmsSent();
						
						smsState_ = SmsState.SmsSent;
					}			
					break;
					
				case Modem.SmsState.SmsSent: //do nothing
					break;
					
				case Modem.SmsState.SmsReceived:
					if(fullReceivedLine_.Contains(SMS_READEN_RESPONSE))
					{
						ParseReadenSmsHeader();
						smsContents_.Clear();
						needToParseSmsContents_ = true;
						smsState_ = SmsState.SmsReaden;
					}					
					break;
					
				case SmsState.SmsReaden:
					smsState_ = SmsState.SmsHandled;
					break;
					
				case Modem.SmsState.SmsHandled:			
					smsState_ = SmsState.Unknown; //reset to next sms					
					break;
					
				default:
					throw new Exception("Invalid value for SmsState");
			}			
		}
		
		void SetSmsHandled()
		{
			if(View != null)
				View.SetSmsHandledCheckBox();
			if(ModemView != null)
				ModemView.SetSmsHandled();			
		}
		
		void ParseSMSReadenResponse()
		{
			if(fullReceivedLine_.Contains(SMS_READEN_RESPONSE))
			{
				int pos = fullReceivedLine_.IndexOf("| | OK|");
				if(pos < 0)
				{
					LogText("Partial received CMGR");
					
					return;
				}
				string response = fullReceivedLine_.Substring(pos - 3, 3);
				LogText(response);
								
				ParseSmsResponse(response);			
				
				SetSmsHandled();
			}
		}
		
		void HandleReceivedSms()
		{
			//Response should contain: +CMTI: "SM",1
			if(!fullReceivedLine_.Contains(SMS_RECEIVED_RESPONSE))
			{
				LogText("Unable to handle sms in text: " + ReceivedLine);				
				
				return;
			}
			
			int qi = fullReceivedLine_.IndexOf("\"SM\",");
			if( qi == -1 )
			{
				LogText("Error: Unable to find sms no");
				
				return;
			}
			
			qi += 5;
			
			int lastCR = fullReceivedLine_.LastIndexOf('|');
			
			string smsNo = fullReceivedLine_.Substring( qi, lastCR - qi );			

			if (smsNo == null)
				return;
			
			LogText("SMS at " + smsNo);
						
			if(smsNo.Contains("|"))			
			{
				int barPlace = smsNo.IndexOf('|');
				smsNo = smsNo.Substring(0, barPlace - 1);
			}
			
			int smsNumber = Convert.ToInt16(smsNo); 
			ReadSmsAt(smsNumber);
			
			if(smsNumber > MAX_SMS)
			{
				needToRemoveAllSms_ = true;
			}
		}
		
		public void ReadSmsAt( int smsIndex )
		{
			atCommands_.Clear();
			atCommands_.Add("AT+CMGR=" + smsIndex.ToString() + Environment.NewLine);
			StartCommandSend();			
		}
		
		void HandleReceivedLine()
		{
			if(ReceivedLine.Contains(OK_RESPONSE))
			{
				HandleOkResponse();
			}
			else if(ReceivedLine.Contains(ERROR_RESPONSE))
			{
				HandleErrorResponse();
			}			
		}

		public void SendNextSmsCommand()
		{			
			curSmsCommand += 1;	
			if( curSmsCommand >= smsCommands_.Count )
			{
				LogText("No more sms commands");
								
				return;
			}

			SendToSerial( smsCommands_[curSmsCommand] );
			
			ClearSmsStatus();
		}
		
		void ClearSmsStatus()
		{
			if( View != null )
				View.ClearSmsStatusUI();
			
			if( ModemView != null )
				ModemView.ClearSmsStatus();			
		}

		void ParseReadenSmsHeader()
		{
			//todo: Parse it from response
			//  +CMGR: "REC UNREAD","+77022136653","","18/04/23,11:16:38+24"
		}

		public void HandleOkResponse()
		{
			if(needToParseSmsContents_)
			{
				if(smsContents_.Count > 1)
				{
					LogText("SMS Contents: " + smsContents_[1]);	
				}
			
				needToParseSmsContents_ = false;
				ParseSmsContents();			
				smsContents_.Clear();

				SetSmsHandled();
			}
			
			LogText(">>OK");			
			
			SendNextCommand();
		}	
		
		public void HandleErrorResponse()
		{
			LogText(">>Error");						
		}
		
		void ParseSmsContents()
		{
			if(smsContents_.Count < 2)
			{
				LogText("Error: Sms does not contain parsable content\n");
				
				return;
			}
			
			string response = smsContents_[0];//First line contains: 100, AU/RU, DV, AV response
					
			ParseSmsResponse(response);
		}
		
		void SetAutomaticMode()
		{
			if(View != null)
				View.SetAutomaticModeCheckbox();
			
			if(ModemView != null)
				ModemView.SetAutomaticMode();		
		}
		
		void SetLatchValue(int value)
		{
			if(View != null)
				View.SetLatchValue(value);
			
			if(ModemView != null)
				ModemView.SetLatchValue(value);
		}
		
		void SetManualMode()
		{
			if(View != null)
				View.SetManualModeCheckbox();
			
			if(ModemView != null)
				ModemView.SetManualMode();
		}
		
		void SetDoorOpenAlarm()
		{
			if(View != null)
				View.SetDoorOpenAlarmCheckbox();
			
			if(ModemView != null)
				ModemView.SetDoorOpenAlarm();
		}
		
		void SetControlMotorAlarm()
		{
			if(View != null)
				View.SetControlMotorAlarmCheckbox();
			if(ModemView != null)
				ModemView.SetControlMotorAlarm();		
		}
		
		void SetEAAlarm()
		{
			if(View != null)
				View.SetEAAlarmCheckbox();
			if(ModemView != null)
				ModemView.SetEAAlarm();		
		}
		
		void ParseSmsResponse(string response)
		{
			int value;
			if(int.TryParse(response, out value))
			{
				SetLatchValue(value);
				return;
			}	
			
			response = response.TrimStart(' ');

			if(response == "AU")
			{
				SetAutomaticMode();
				
				return;
			}
			if(response == "RU")
			{
				SetManualMode();
				
				return;
			}
			
			if(response == "Dv")
			{
				SetDoorOpenAlarm();
				
				return;
			}
			
			if(response == "AV")
			{
				SetControlMotorAlarm();
				
				return;
			}
			
			if(response == "EA")
			{
				SetEAAlarm();
				
				return;
			}
		}
		
		void StartCommandSend(){
			curCommand = -1;
			SendNextCommand();
		}
		
		void SendNextCommand()
		{
			curCommand += 1;
			if(curCommand >= atCommands_.Count)
			{
				LogText("!! No more commands");
								
				return;
			}

			fullReceivedLine_ = ""; //we need to read all response from this command and see what's next			
			SendToSerial(atCommands_[curCommand]);
		}

		public void ReadAllSms()
		{
			atCommands_.Clear();
			atCommands_.Add("AT+CMGL=\"ALL\"\r\n");
			StartCommandSend();			
		}
		
		public void SendSms( string msg )
		{
			curSmsCommand = -1;
			ResetSmsState();
			SendSmsInternal(msg);
		}
		
		public void SendSmsWithCR( string msg )
		{
			SendSms( msg + "\r\n" );
		}
		
		public void RemoveAllSms()
		{
			atCommands_.Clear();
			atCommands_.Add("AT+CMGD=1,4" + Environment.NewLine);
			StartCommandSend();
		}
		
		private string smsNumber_ = "87022136653";
		private bool configSet = false;
		
		void SendSmsInternal(string msg)
		{
			atCommands_.Clear();
			if(!configSet)
			{
				atCommands_.Add("AT+CMGF=1" + Environment.NewLine );
				configSet = true;
			}
			
			atCommands_.Add(String.Format("AT+CMGS=\"{0}\"\r\n", smsNumber_));
			
			smsCommands_.Clear();
			smsCommands_.Add(msg + (char)(26) );
			StartCommandSend();
		}		
		
		public void InitPort()
		{
			Port.DataReceived += new SerialDataReceivedEventHandler(Port_DataReceived);			
		}

		void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
		 	dataReceived_ = true;
		 	
			if(e.EventType == SerialData.Chars)
			{
				string recv = Port.ReadExisting(); 
				HandleReceived(recv);
			}
			else if(e.EventType == SerialData.Eof)
			{
				LogText(" .EOF. ");				
			}
		}
		
		public void ResetSmsState()
		{
			smsState_ = SmsState.Unknown;
		}
		
		public bool IsNewSmsState()
		{
			return smsState_ == SmsState.NewSms;
		}
		
		public void WaitUntilReceived()
		{
			const int MAX_COUNT = 30;
			
			int count = 0;
			while(!dataReceived_)
			{
				Thread.Sleep(100);
				
				if(++count > MAX_COUNT)
				{
					throw new TimeoutException("Data do not received from port");
				}
			}
		}
	}
}

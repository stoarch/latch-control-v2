﻿/*
 * Created by SharpDevelop.
 * User: 1
 * Date: 01.09.2018
 * Time: 12:41
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using FosforLatch.Communication;
using FosforLatch.Communication.Interfaces;
using FosforLatch.Interfaces;
using IO.Modems.Interfaces;
using NUnit.Framework;
using Telerik.JustMock;

namespace Tests
{
	[TestFixture]
	public class LatchSmsDriverTest
	{
		[Test]
		public void ItShouldBeCreated()
		{
			var driver = new LatchSmsDriver();
			
			Assert.IsNotNull(driver);
		}
		
		[Test]
		public void ItShouldSendSms()
		{
			var driver = new LatchSmsDriver();
			var smsNo = "87022136653";
			
			var modem = Mock.Create<ISmsProvider>();
			Mock.Arrange(() => modem.SendSms(smsNo, "0"))
				.MustBeCalled();
			
			driver.SmsProvider = modem;
			Assert.AreEqual(modem, driver.SmsProvider);
			
			driver.SendSms(smsNo, "0");
			
			Mock.Assert(modem);
		}
		
		[Test]
		public void ItShouldReceiveSms()
		{
			var driver = new LatchSmsDriver();
			var smsNum = "87022136653";
			var smsValue = "100";
			
			var modem = Mock.Create<ISmsProvider>();
			Mock.Arrange(() => modem.SendSms(smsNum, smsValue))
				.Raises(() => modem.SmsReceived += null, smsNum, smsValue);
			
			string asmsNo = "", asmsText = "";
			
			modem.SmsReceived += (smsNo, smsText) => { asmsNo = smsNo; asmsText = smsText; };
			
			driver.SmsProvider = modem;
			
			driver.SendSms(smsNum, smsValue);
			
			Assert.AreEqual(smsNum, asmsNo);
			Assert.AreEqual(smsValue, asmsText);
		}
		
		[Test]
		public void ItShouldSendStateSmsAndReceiveLatchStatus()
		{
			var driver = new LatchSmsDriver();
			var smsNum = "87022136653";
			var smsValue = "state";
			var latchValue = "100";
			
			var modem = Mock.Create<ISmsProvider>();
			Mock.Arrange(() => modem.SendSms(smsNum, smsValue))
				.Raises(() => modem.SmsReceived += null, smsNum, latchValue)
				.MustBeCalled();
			
			string asmsNo = "", asmsText = "";
			
			modem.SmsReceived += (smsNo, smsText) => { asmsNo = smsNo; asmsText = smsText; };
			
			driver.SmsProvider = modem;		

			driver.ReceiveState(smsNum);

			Mock.Assert(modem);
			Assert.AreEqual(latchValue, asmsText);
		}

		[Test]
		public void ItShouldResetSmsAndReceiveLatchStatus()
		{
			var driver = new LatchSmsDriver();
			var smsNum = "87022136653";
			var smsValue = "reset";
			var latchValue = "100";
			
			var modem = Mock.Create<ISmsProvider>();
			Mock.Arrange(() => modem.SendSms(smsNum, smsValue))
				.MustBeCalled();		
		
			driver.SmsProvider = modem;		

			driver.ResetLatch(smsNum);

			Mock.Assert(modem);
		}

		[Test]
		public void ItShouldReceiveStateByIdAndReceiveLatchStatus()
		{
			var driver = new LatchSmsDriver();
			var idriver = driver as ILatchDriver;
			var smsNum = "+77022136653";
			int latchId = 0;
			var latchValue = 100;
			var smsValue = latchValue.ToString();
			var smsCommand = "state";
			
			var modem = Mock.Create<ISmsProvider>();
			Mock.Arrange(() => modem.SendSms(smsNum, smsCommand))
				.Raises(() => modem.SmsReceived += null, smsNum, smsValue)
				.MustBeCalled();
			
			string asmsNo = "", asmsText = "";
			
			modem.SmsReceived += (smsNo, smsText) => { asmsNo = smsNo; asmsText = smsText; };
			
			driver.SmsProvider = modem;

			bool isStateReceived = false;
			int valueReceived = 0;
			
			driver.LatchStateReceived += delegate(object sender, LatchValueEventArgs args) {
				isStateReceived = true;
				valueReceived = args.Value;				
			};

			//ACT//
			idriver.ReceiveState(latchId);

			//ASSERT//
			Assert.IsTrue(isStateReceived, "No state received");
			Mock.Assert(modem, "Modem not called");
			Assert.AreEqual(smsNum, asmsNo, "Invalid sms num used");
			Assert.AreEqual(smsValue, asmsText, "Sms received invalid");
			Assert.AreEqual(latchValue, valueReceived, "Invalid value received");
		}
		
		[Test]
		public void ItShouldSendLatchStateChangeSmsAndReceiveLatchStatus()
		{
			var driver = new LatchSmsDriver();
			var smsNum = "87022136653";
			var newLatchValue = 12;
			var receivedLatchValue = "12";
			
			var modem = Mock.Create<ISmsProvider>();
			Mock.Arrange(() => modem.SendSms(smsNum, newLatchValue.ToString()))
				.Raises(() => modem.SmsReceived += null, smsNum, receivedLatchValue)
				.MustBeCalled();
			
			string asmsNo = "", asmsText = "";
			
			modem.SmsReceived += (smsNo, smsText) => { asmsNo = smsNo; asmsText = smsText; };
			
			driver.SmsProvider = modem;		

			driver.SendState(smsNum, newLatchValue);

			Mock.Assert(modem);
			Assert.AreEqual(receivedLatchValue, asmsText);
		}
		
		[Test]
		public void ItShouldReceiveLatchStateSmsFromProviderAndThrowLatchStateReceivedEvent()
		{
			var driver = new LatchSmsDriver();
			var smsNum = "+77022136653";
			var smsValue = "state";
			var oldLatchValue = 100;
			
			var modem = Mock.Create<ISmsProvider>();
			Mock.Arrange(() => modem.SendSms(smsNum, smsValue))
				.Raises(() => modem.SmsReceived += null, smsNum, oldLatchValue.ToString());
					
			driver.SmsProvider = modem;		
			
			int newLatchValue = -1;
			string smsNo = "";
			int aid = -1;
			
			driver.LatchStateReceived += (sender, latchEvtArgs) => { 
				smsNo = latchEvtArgs.SmsNo; 
				newLatchValue = latchEvtArgs.Value;
				aid = latchEvtArgs.Id;
			};

			driver.ReceiveState(smsNum);
		
			Assert.AreEqual(0, aid, "Invalid id latch received");
			Assert.AreEqual(oldLatchValue, newLatchValue);		
			Assert.AreEqual(smsNum, smsNo);
		}
		
		[Test]
		public void ItShouldParseAutomaticModeSmsAndSend()
		{
			var driver = new LatchSmsDriver();
			var parser = driver as ISmsParser;
			var smsNum = "+77022136653";
			var smsValue = "AU";
			
			bool autoModeFired = false;
			string smsFired = "";
			
			driver.AutomaticModeSet += delegate(object sender, LatchEventArgs args) {
				autoModeFired = true;					
				smsFired = args.SmsNo;
			};
			
			parser.ParseSms(smsNum, smsValue);
			
			Assert.IsTrue(autoModeFired);
			Assert.AreEqual(smsNum, smsFired);
		}
		
		[Test]
		public void ItShouldParseLatchValueSmsAndSend()
		{
			var driver = new LatchSmsDriver();
			var parser = driver as ISmsParser;
			var smsNum = "+77022136653";
			var smsValue = "100";
			int value = Convert.ToInt32(smsValue);
			
			bool isFired = false;
			string smsFired = "";
			int valueReceived = 0;
			
			driver.LatchStateReceived += delegate(object sender, LatchValueEventArgs args) {
				isFired = true;					
				smsFired = args.SmsNo;
				valueReceived = args.Value;
			};
			
			parser.ParseSms(smsNum, smsValue);
			
			Assert.IsTrue(isFired);
			Assert.AreEqual(smsNum, smsFired);
			Assert.AreEqual(value, valueReceived);
		}	
		
		[Test]
		public void ItShouldParseManualModeSmsAndSend()
		{
			var driver = new LatchSmsDriver();
			var parser = driver as ISmsParser;
			var smsNum = "+77022136653";
			var smsValue = "RU";
			
			bool isFired = false;
			string smsFired = "";
			
			driver.ManualModeSet += delegate(object sender, LatchEventArgs args) {
				isFired = true;					
				smsFired = args.SmsNo;
			};
			
			parser.ParseSms(smsNum, smsValue);
			
			Assert.IsTrue(isFired);
			Assert.AreEqual(smsNum, smsFired);
		}
		
		[Test]
		public void ItShouldParseDoorOpenAlarmSmsAndSend()
		{
			var driver = new LatchSmsDriver();
			var parser = driver as ISmsParser;
			var smsNum = "+77022136653";
			var smsValue = "Dv";
			
			bool isFired = false;
			string smsFired = "";
			
			driver.AlarmDoorOpenSet += delegate(object sender, LatchEventArgs args) {
				isFired = true;					
				smsFired = args.SmsNo;
			};
			
			parser.ParseSms(smsNum, smsValue);
			
			Assert.IsTrue(isFired);
			Assert.AreEqual(smsNum, smsFired);
		}
		
		[Test]
		public void ItShouldParseControlMotorAlarmSmsAndSend()
		{
			var driver = new LatchSmsDriver();
			var parser = driver as ISmsParser;
			var smsNum = "+77022136653";
			var smsValue = "AV";
			
			bool isFired = false;
			string smsFired = "";
			
			driver.AlarmMotorErrorSet += delegate(object sender, LatchEventArgs args) {
				isFired = true;					
				smsFired = args.SmsNo;
			};
			
			parser.ParseSms(smsNum, smsValue);
			
			Assert.IsTrue(isFired);
			Assert.AreEqual(smsNum, smsFired);
		}

		[Test]
		public void ItShouldParseEAAlarmSmsAndSend()
		{
			var driver = new LatchSmsDriver();
			var parser = driver as ISmsParser;
			var smsNum = "+77022136653";
			var smsValue = "EA";
			
			bool isFired = false;
			string smsFired = "";
			
			driver.AlarmEASet += delegate(object sender, LatchEventArgs args) {
				isFired = true;					
				smsFired = args.SmsNo;
			};
			
			parser.ParseSms(smsNum, smsValue);
			
			Assert.IsTrue(isFired);
			Assert.AreEqual(smsNum, smsFired);
		}
		
		[Test]
		public void ItShouldGetSmsByLatchId()
		{
			var driver = new LatchSmsDriver();
			
			Assert.AreEqual("+77022136653", driver.GetSmsById(0));
			Assert.AreEqual("+77786415240", driver.GetSmsById(1));
		}
		
		[Test]
		public void ItShouldGetIdFromSms()
		{
			var driver = new LatchSmsDriver();
			
			Assert.AreEqual(0, driver.GetIdBySms("+77022136653"));
			Assert.AreEqual(1, driver.GetIdBySms("+77786415240"));			
		}
		
		[Test]
		public void ItShouldParseOneLineSms()
		{
			Assert.Ignore("Not implemented");
			string smsOneLine = " Full received: AT+CMGR=18| | +CMGR: \"REC UNREAD\",\"+77022136653\",\"\",\"18/09/07,11:54:09+24\"| 0| | OK| ";
		}
		
		
	}
}

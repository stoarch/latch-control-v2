﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 04.09.2018
 * Time: 11:35
 *
 */
using System;
using FosforLatch;
using NUnit.Framework;

namespace Tests
{
	[TestFixture]
	public class ModemHandlerTest
	{
		[Test]
		public void ItShouldParseSmsHeader()
		{
			var modem = new Modem();
			const string SMS_HEADER = "+CMGR: \"REC UNREAD\",\"+77022136653\",\"\",\"18/04/23,11:16:38+24\"";
			
			var expected = "+77022136653";
			var result = modem.ParseSmsHeader(SMS_HEADER);
			
			Assert.AreEqual(expected, result);			
		}
	}
}

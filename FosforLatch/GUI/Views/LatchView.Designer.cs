﻿/*
 * Created by SharpDevelop.
 * User: 1
 * Date: 05.09.2018
 * Time: 10:25
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace FosforLatch.GUI.Views
{
	partial class LatchView
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LatchView));
			this.receivedLatchValue_label = new System.Windows.Forms.Label();
			this.latchState_vScrollBar = new System.Windows.Forms.VScrollBar();
			this.labelLatchCaption = new System.Windows.Forms.Label();
			this.eaAlarm_checkBox = new System.Windows.Forms.CheckBox();
			this.autoMode_checkBox = new System.Windows.Forms.CheckBox();
			this.manualMode_checkBox = new System.Windows.Forms.CheckBox();
			this.label15 = new System.Windows.Forms.Label();
			this.controlMotorAlarm_checkBox = new System.Windows.Forms.CheckBox();
			this.doorOpenAlarm_checkBox = new System.Windows.Forms.CheckBox();
			this.label14 = new System.Windows.Forms.Label();
			this.valve_pictureBox = new System.Windows.Forms.PictureBox();
			this.clearAlarms_button = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.valve_pictureBox)).BeginInit();
			this.SuspendLayout();
			// 
			// receivedLatchValue_label
			// 
			this.receivedLatchValue_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.receivedLatchValue_label.ForeColor = System.Drawing.Color.Blue;
			this.receivedLatchValue_label.Location = new System.Drawing.Point(9, 46);
			this.receivedLatchValue_label.Name = "receivedLatchValue_label";
			this.receivedLatchValue_label.Size = new System.Drawing.Size(75, 51);
			this.receivedLatchValue_label.TabIndex = 40;
			this.receivedLatchValue_label.Text = "100";
			this.receivedLatchValue_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// latchState_vScrollBar
			// 
			this.latchState_vScrollBar.Location = new System.Drawing.Point(9, 102);
			this.latchState_vScrollBar.Name = "latchState_vScrollBar";
			this.latchState_vScrollBar.Size = new System.Drawing.Size(75, 278);
			this.latchState_vScrollBar.TabIndex = 39;
			this.latchState_vScrollBar.Value = 90;
			// 
			// labelLatchCaption
			// 
			this.labelLatchCaption.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.labelLatchCaption.Font = new System.Drawing.Font("Consolas", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelLatchCaption.Location = new System.Drawing.Point(9, 13);
			this.labelLatchCaption.Name = "labelLatchCaption";
			this.labelLatchCaption.Size = new System.Drawing.Size(430, 33);
			this.labelLatchCaption.TabIndex = 41;
			this.labelLatchCaption.Text = "Задвижка: Фосфор";
			// 
			// eaAlarm_checkBox
			// 
			this.eaAlarm_checkBox.AutoCheck = false;
			this.eaAlarm_checkBox.Location = new System.Drawing.Point(284, 237);
			this.eaAlarm_checkBox.Name = "eaAlarm_checkBox";
			this.eaAlarm_checkBox.Size = new System.Drawing.Size(133, 24);
			this.eaAlarm_checkBox.TabIndex = 48;
			this.eaAlarm_checkBox.Text = "Авария автоматики";
			this.eaAlarm_checkBox.UseVisualStyleBackColor = true;
			// 
			// autoMode_checkBox
			// 
			this.autoMode_checkBox.AutoCheck = false;
			this.autoMode_checkBox.Checked = true;
			this.autoMode_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.autoMode_checkBox.Location = new System.Drawing.Point(284, 125);
			this.autoMode_checkBox.Name = "autoMode_checkBox";
			this.autoMode_checkBox.Size = new System.Drawing.Size(114, 24);
			this.autoMode_checkBox.TabIndex = 47;
			this.autoMode_checkBox.Text = "Автоматический";
			this.autoMode_checkBox.UseVisualStyleBackColor = true;
			// 
			// manualMode_checkBox
			// 
			this.manualMode_checkBox.AutoCheck = false;
			this.manualMode_checkBox.Location = new System.Drawing.Point(284, 95);
			this.manualMode_checkBox.Name = "manualMode_checkBox";
			this.manualMode_checkBox.Size = new System.Drawing.Size(104, 24);
			this.manualMode_checkBox.TabIndex = 46;
			this.manualMode_checkBox.Text = "Ручной";
			this.manualMode_checkBox.UseVisualStyleBackColor = true;
			// 
			// label15
			// 
			this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label15.Location = new System.Drawing.Point(284, 69);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(133, 23);
			this.label15.TabIndex = 45;
			this.label15.Text = "Режим работы:";
			// 
			// controlMotorAlarm_checkBox
			// 
			this.controlMotorAlarm_checkBox.AutoCheck = false;
			this.controlMotorAlarm_checkBox.Location = new System.Drawing.Point(284, 207);
			this.controlMotorAlarm_checkBox.Name = "controlMotorAlarm_checkBox";
			this.controlMotorAlarm_checkBox.Size = new System.Drawing.Size(155, 24);
			this.controlMotorAlarm_checkBox.TabIndex = 44;
			this.controlMotorAlarm_checkBox.Text = "Авария движка шкафа";
			this.controlMotorAlarm_checkBox.UseVisualStyleBackColor = true;
			// 
			// doorOpenAlarm_checkBox
			// 
			this.doorOpenAlarm_checkBox.AutoCheck = false;
			this.doorOpenAlarm_checkBox.Location = new System.Drawing.Point(284, 178);
			this.doorOpenAlarm_checkBox.Name = "doorOpenAlarm_checkBox";
			this.doorOpenAlarm_checkBox.Size = new System.Drawing.Size(104, 24);
			this.doorOpenAlarm_checkBox.TabIndex = 43;
			this.doorOpenAlarm_checkBox.Text = "Открыта дверь";
			this.doorOpenAlarm_checkBox.UseVisualStyleBackColor = true;
			// 
			// label14
			// 
			this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label14.ForeColor = System.Drawing.Color.Coral;
			this.label14.Location = new System.Drawing.Point(284, 161);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(100, 17);
			this.label14.TabIndex = 42;
			this.label14.Text = "Аварии:";
			// 
			// valve_pictureBox
			// 
			this.valve_pictureBox.BackColor = System.Drawing.Color.Transparent;
			this.valve_pictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.valve_pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("valve_pictureBox.Image")));
			this.valve_pictureBox.Location = new System.Drawing.Point(113, 102);
			this.valve_pictureBox.Name = "valve_pictureBox";
			this.valve_pictureBox.Size = new System.Drawing.Size(149, 278);
			this.valve_pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.valve_pictureBox.TabIndex = 49;
			this.valve_pictureBox.TabStop = false;
			// 
			// clearAlarms_button
			// 
			this.clearAlarms_button.Location = new System.Drawing.Point(284, 267);
			this.clearAlarms_button.Name = "clearAlarms_button";
			this.clearAlarms_button.Size = new System.Drawing.Size(133, 34);
			this.clearAlarms_button.TabIndex = 50;
			this.clearAlarms_button.Text = "Очистить аварии";
			this.clearAlarms_button.UseVisualStyleBackColor = true;
			this.clearAlarms_button.Click += new System.EventHandler(this.ClearAlarms_buttonClick);
			// 
			// LatchView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Info;
			this.Controls.Add(this.clearAlarms_button);
			this.Controls.Add(this.valve_pictureBox);
			this.Controls.Add(this.eaAlarm_checkBox);
			this.Controls.Add(this.autoMode_checkBox);
			this.Controls.Add(this.manualMode_checkBox);
			this.Controls.Add(this.label15);
			this.Controls.Add(this.controlMotorAlarm_checkBox);
			this.Controls.Add(this.doorOpenAlarm_checkBox);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.labelLatchCaption);
			this.Controls.Add(this.receivedLatchValue_label);
			this.Controls.Add(this.latchState_vScrollBar);
			this.Name = "LatchView";
			this.Size = new System.Drawing.Size(445, 394);
			((System.ComponentModel.ISupportInitialize)(this.valve_pictureBox)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Button clearAlarms_button;
		private System.Windows.Forms.PictureBox valve_pictureBox;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.CheckBox doorOpenAlarm_checkBox;
		private System.Windows.Forms.CheckBox controlMotorAlarm_checkBox;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.CheckBox manualMode_checkBox;
		private System.Windows.Forms.CheckBox autoMode_checkBox;
		private System.Windows.Forms.CheckBox eaAlarm_checkBox;
		private System.Windows.Forms.Label labelLatchCaption;
		private System.Windows.Forms.VScrollBar latchState_vScrollBar;
		private System.Windows.Forms.Label receivedLatchValue_label;
	}
}

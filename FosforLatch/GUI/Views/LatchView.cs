﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 05.09.2018
 * Time: 10:25
 * 
 */
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

using FosforLatch.Interfaces;

namespace FosforLatch.GUI.Views
{
	/// <summary>
	/// Description of LatchView.
	/// </summary>
	public partial class LatchView : UserControl, ILatchView
	{
		readonly string exeLocation_;				
		
		[Description("Caption of view"), Category("Latch")]
		[Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
		public string Caption
		{
			get
			{
				return labelLatchCaption.Text;	
			}
			set
			{
				labelLatchCaption.Text = value;
			}
		}
		
		public LatchView()
		{			
			InitializeComponent();			

			try
			{
				exeLocation_ = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			} catch( Exception ex ){
				Console.WriteLine("Error:" + ex.Message);
				Debug.WriteLine("Error: " + ex.Message );
			}
									
		}
		
		int GetOpenImageIndexFor(int value)
		{
			if(value <= 10) return 11;//6;
			if(value <= 30) return 10;//8;
			if(value <= 60) return 9;//9;
			if(value <= 90) return 8;//10;
			if(value > 90) return 6;//11;
			
			return 6;
		}
		
		void LoadOpenImage(int index)
		{
			try
			{
			string fileName = Path.Combine(exeLocation_, String.Format("Images\\opening\\opening ({0}).jpg", index));
			
			valve_pictureBox.ImageLocation = fileName;
			}catch(Exception ex){
				Console.WriteLine("Error: " + ex.Message );
				Debug.WriteLine("Error: " + ex.Message );
			}
		}

		delegate void HandlerCallback();

		public void SetAutomaticModeCheckbox()
		{
			if(autoMode_checkBox.InvokeRequired)
			{
				HandlerCallback d = SetAutomaticModeCheckbox;
				Invoke(d);
				return;
			}
			
			autoMode_checkBox.Checked = true;
			manualMode_checkBox.Checked = false;
		}

		public void SetManualModeCheckbox()
		{
			if(manualMode_checkBox.InvokeRequired)
			{
				HandlerCallback d = SetManualModeCheckbox;
				Invoke(d);
				return;
			}
			
			autoMode_checkBox.Checked = false;
			manualMode_checkBox.Checked = true;
		}
		
		public void SetDoorOpenAlarmCheckbox()
		{
			if(doorOpenAlarm_checkBox.InvokeRequired)
			{
				HandlerCallback d = SetDoorOpenAlarmCheckbox;
				Invoke(d);
				return;
			}
			doorOpenAlarm_checkBox.Checked = true;
		}
		
		public void SetControlMotorAlarmCheckbox()
		{
			if(controlMotorAlarm_checkBox.InvokeRequired)
			{
				HandlerCallback d = SetControlMotorAlarmCheckbox;
				Invoke(d);
				return;
			}
			controlMotorAlarm_checkBox.Checked = true;
		}
		
		public void SetEAAlarmCheckbox()
		{
			if(eaAlarm_checkBox.InvokeRequired)
			{
				HandlerCallback d = SetEAAlarmCheckbox;
				Invoke(d);
				return;
			}
			eaAlarm_checkBox.Checked = true;
		}

		delegate void SetTextCallback(string text);
		
		void SetReceivedLatchValueText(string value)
		{
			if(receivedLatchValue_label.InvokeRequired)
			{
				SetTextCallback d = new SetTextCallback(SetReceivedLatchValueText);
				Invoke(d, new object[]{value});
				
				return;
			}
			
			receivedLatchValue_label.Text = value;	
		}
		
		delegate void SetValue(int value);
		
		void SetLatchStateView(int value)
		{
			if(latchState_vScrollBar.InvokeRequired)
			{
				var d = new SetValue(SetLatchStateView);
				Invoke(d, new object[]{value});
				return;
			}
			latchState_vScrollBar.Value = 100 - value;
			UpdateLatchStateView();
		}
		
		void UpdateLatchStateView()
		{
			int value = (latchState_vScrollBar.Value);
			int index = GetOpenImageIndexFor(value);
			
			LoadOpenImage( index );			
		}
		
		public void SetLatchValue(int value)
		{
			if(value < 0)
			{
				value = 0;
			}
			
			if(value > 100)
			{
				value = 100;
			}
		
			SetReceivedLatchValueText(value.ToString());
			SetLatchStateView(value);
		}
		
		void LatchState_vScrollBarScroll(object sender, ScrollEventArgs e)
		{
			UpdateLatchStateView();
		}
		
		public void SetLatchValue(float value)
		{
			SetLatchValue((int)value);
		}
		
		public void SetAutomaticMode()
		{
			SetAutomaticModeCheckbox();
		}
		
		public void SetManualMode()
		{
			SetManualModeCheckbox();
		}
		
		public void SetDoorOpenAlarm()
		{
			SetDoorOpenAlarmCheckbox();
		}
		
		public void SetControlMotorAlarm()
		{
			SetControlMotorAlarmCheckbox();
		}
		
		public void SetEAAlarm()
		{
			SetEAAlarmCheckbox();
		}
		
		void ClearAlarms_buttonClick(object sender, EventArgs e)
		{
			eaAlarm_checkBox.Checked = false;	
			doorOpenAlarm_checkBox.Checked = false;
			controlMotorAlarm_checkBox.Checked = false;			
		}
	}	
}

﻿/*
 * Created by SharpDevelop.
 * User: 1
 * Date: 05.09.2018
 * Time: 10:32
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using FosforLatch.Latch;

namespace FosforLatch.GUI.Controls
{
	public class LatchLevelEventArgs : EventArgs
	{
		public int Value {get; private set;}
		
		public LatchLevelEventArgs(int value) : base(){
			Value = value;
		}
	}

	public delegate void LatchLevelEventHandler(object sender, LatchLevelEventArgs args);
	
	/// <summary>
	/// Description of LatchLevelControl.
	/// </summary>
	public partial class LatchLevelControl : UserControl
	{
		int latchValue_ = 0;
		LatchWorkRange latchRange_;

		[Description("Min avail value"), Category("Latch")]
		[Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
		public int Min
		{
			get
			{
				return latchRange_.Min;
			}
			set
			{
				latchRange_.Min = value;
				UpdateRangeView();
			}
		}

		[Description("Max avail value"), Category("Latch")]
		[Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
		public int Max
		{
			get
			{
				return latchRange_.Max;
			}
			set
			{
				latchRange_.Max = value;
				UpdateRangeView();
			}
		}
		
		void UpdateRangeView()
		{
			if( latch_trackBar.Value < 100 - latchRange_.Max )
			{
				latch_trackBar.Value = 100 - latchRange_.Max;
			}
			
			if(latch_trackBar.Value > 100 - latchRange_.Min)
			{
				latch_trackBar.Value = 100 - latchRange_.Min;
			}
		}
		
		bool IsLatchInRange()
		{
			return ( latch_trackBar.Value >= 100 - latchRange_.Max ) &&
				(latch_trackBar.Value <= 100 - latchRange_.Min);
		}
		
		public LatchLevelControl()
		{
			InitializeComponent();			
			latchRange_ = new LatchWorkRange();
			latchRange_.Min = 0;
			latchRange_.Max = 100;//percent
		}
		
		public event LatchLevelEventHandler SetLevelEvent;
		public event EventHandler ReadLevelEvent;
		public event EventHandler ResetEvent;
		
		void Latch_trackBarValueChanged(object sender, EventArgs e)
		{
			if(!IsLatchInRange())
			{
				UpdateRangeView();
			}
			latchValue_ = 100 - latch_trackBar.Value;	
			newLatchState_label.Text = latchValue_.ToString();
		}
					
		void SendNewLatchValue_buttonClick(object sender, EventArgs e)
		{
			OnLatchSetLevel(latchValue_);
		}
		
		void OnLatchSetLevel(int value)
		{
			if(SetLevelEvent != null)
				SetLevelEvent(this, new LatchLevelEventArgs(value));
		}
		
		void ButtonReadStateClick(object sender, EventArgs e)
		{
			OnLatchReadLevel();
		}
		
		void OnLatchReadLevel()
		{
			if(ReadLevelEvent != null)
			{
				ReadLevelEvent(this, EventArgs.Empty);
			}
		}
		
		void ButtonResetClick(object sender, EventArgs e)
		{
			OnReset();
		}
		
		void OnReset()
		{
			if(ResetEvent != null)
			{
				ResetEvent(this, EventArgs.Empty);
			}
		}
	}
}

﻿/*
 * Created by SharpDevelop.
 * User: 1
 * Date: 05.09.2018
 * Time: 10:32
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace FosforLatch.GUI.Controls
{
	partial class LatchLevelControl
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.newLatchState_label = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.buttonReadState = new System.Windows.Forms.Button();
			this.label12 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.sendNewLatchValue_button = new System.Windows.Forms.Button();
			this.latch_trackBar = new System.Windows.Forms.TrackBar();
			this.label1 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.buttonReset = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.latch_trackBar)).BeginInit();
			this.SuspendLayout();
			// 
			// newLatchState_label
			// 
			this.newLatchState_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.newLatchState_label.Location = new System.Drawing.Point(3, 31);
			this.newLatchState_label.Name = "newLatchState_label";
			this.newLatchState_label.Size = new System.Drawing.Size(77, 51);
			this.newLatchState_label.TabIndex = 53;
			this.newLatchState_label.Text = "99";
			this.newLatchState_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label18
			// 
			this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label18.Location = new System.Drawing.Point(3, 14);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(77, 19);
			this.label18.TabIndex = 55;
			this.label18.Text = "Уставка";
			this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// buttonReadState
			// 
			this.buttonReadState.Location = new System.Drawing.Point(95, 99);
			this.buttonReadState.Name = "buttonReadState";
			this.buttonReadState.Size = new System.Drawing.Size(95, 33);
			this.buttonReadState.TabIndex = 54;
			this.buttonReadState.Text = "Прочесть %";
			this.buttonReadState.UseVisualStyleBackColor = true;
			this.buttonReadState.Click += new System.EventHandler(this.ButtonReadStateClick);
			// 
			// label12
			// 
			this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label12.Location = new System.Drawing.Point(86, 9);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(38, 19);
			this.label12.TabIndex = 52;
			this.label12.Text = "Открыто";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label11
			// 
			this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label11.Location = new System.Drawing.Point(113, 63);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(27, 19);
			this.label11.TabIndex = 51;
			this.label11.Text = "90";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label10
			// 
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label10.Location = new System.Drawing.Point(146, 63);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(27, 19);
			this.label10.TabIndex = 50;
			this.label10.Text = "80";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label9
			// 
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label9.Location = new System.Drawing.Point(179, 63);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(27, 19);
			this.label9.TabIndex = 49;
			this.label9.Text = "70";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label8.Location = new System.Drawing.Point(212, 63);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(27, 19);
			this.label8.TabIndex = 48;
			this.label8.Text = "60";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label7.Location = new System.Drawing.Point(245, 63);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(27, 19);
			this.label7.TabIndex = 47;
			this.label7.Text = "50";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label6.Location = new System.Drawing.Point(278, 63);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(27, 19);
			this.label6.TabIndex = 46;
			this.label6.Text = "40";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label5.Location = new System.Drawing.Point(311, 63);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(27, 19);
			this.label5.TabIndex = 45;
			this.label5.Text = "30";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label4.Location = new System.Drawing.Point(410, 63);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(27, 19);
			this.label4.TabIndex = 44;
			this.label4.Text = "0";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label3.Location = new System.Drawing.Point(344, 63);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(27, 19);
			this.label3.TabIndex = 43;
			this.label3.Text = "20";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label2.Location = new System.Drawing.Point(377, 63);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(27, 19);
			this.label2.TabIndex = 42;
			this.label2.Text = "10";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// sendNewLatchValue_button
			// 
			this.sendNewLatchValue_button.Location = new System.Drawing.Point(3, 99);
			this.sendNewLatchValue_button.Name = "sendNewLatchValue_button";
			this.sendNewLatchValue_button.Size = new System.Drawing.Size(86, 33);
			this.sendNewLatchValue_button.TabIndex = 41;
			this.sendNewLatchValue_button.Text = "Установить";
			this.sendNewLatchValue_button.UseVisualStyleBackColor = true;
			this.sendNewLatchValue_button.Click += new System.EventHandler(this.SendNewLatchValue_buttonClick);
			// 
			// latch_trackBar
			// 
			this.latch_trackBar.Location = new System.Drawing.Point(86, 31);
			this.latch_trackBar.Maximum = 100;
			this.latch_trackBar.Name = "latch_trackBar";
			this.latch_trackBar.Size = new System.Drawing.Size(351, 45);
			this.latch_trackBar.TabIndex = 40;
			this.latch_trackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
			this.latch_trackBar.Value = 1;
			this.latch_trackBar.ValueChanged += new System.EventHandler(this.Latch_trackBarValueChanged);
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(179, 6);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(177, 22);
			this.label1.TabIndex = 56;
			this.label1.Text = "Процент открытия";
			// 
			// label13
			// 
			this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label13.Location = new System.Drawing.Point(399, 8);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(38, 19);
			this.label13.TabIndex = 57;
			this.label13.Text = "Закрыто";
			this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// buttonReset
			// 
			this.buttonReset.Location = new System.Drawing.Point(342, 102);
			this.buttonReset.Name = "buttonReset";
			this.buttonReset.Size = new System.Drawing.Size(95, 33);
			this.buttonReset.TabIndex = 58;
			this.buttonReset.Text = "Сбросить";
			this.buttonReset.UseVisualStyleBackColor = true;
			this.buttonReset.Click += new System.EventHandler(this.ButtonResetClick);
			// 
			// LatchLevelControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.buttonReset);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.newLatchState_label);
			this.Controls.Add(this.label18);
			this.Controls.Add(this.buttonReadState);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.sendNewLatchValue_button);
			this.Controls.Add(this.latch_trackBar);
			this.Name = "LatchLevelControl";
			this.Size = new System.Drawing.Size(447, 135);
			((System.ComponentModel.ISupportInitialize)(this.latch_trackBar)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Button buttonReset;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TrackBar latch_trackBar;
		private System.Windows.Forms.Button sendNewLatchValue_button;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Button buttonReadState;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label newLatchState_label;
	}
}

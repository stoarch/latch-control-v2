﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 01.09.2018
 * Time: 13:03
 * 
 */
using System;

namespace FosforLatch.Interfaces
{
	public delegate void SmsEventHandler(string smsNo, string smsText);
	
	/// <summary>
	/// Description of ISmsProvider.
	/// </summary>
	public interface ISmsProvider
	{
		void SendSms(string smsNo);
		
		event SmsEventHandler SmsReceived;
	}
}

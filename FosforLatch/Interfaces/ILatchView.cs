﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 29.08.2018
 * Time: 12:02
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace FosforLatch.Interfaces
{
	/// <summary>
	/// Description of IModemView.
	/// </summary>
	public interface ILatchView
	{
		void SetLatchValue(float value);
		void SetAutomaticMode();
		void SetManualMode();
		void SetDoorOpenAlarm();
		void SetControlMotorAlarm();
		void SetEAAlarm();
	}
}

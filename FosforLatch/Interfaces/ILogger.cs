﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 05.09.2018
 * Time: 12:22
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace FosforLatch.Interfaces
{
	/// <summary>
	/// Description of ILogger.
	/// </summary>
	public interface ILogger
	{
		void LogText(string message);
	}
}

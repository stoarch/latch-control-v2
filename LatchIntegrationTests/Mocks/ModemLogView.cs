﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 29.08.2018
 * Time: 12:12
 *
* Log all queries from modem to view 
 */
using System;
using FosforLatch.Interfaces;
using IO.Modems.Interfaces;

namespace FosforLatch.Mocks
{
	/// <summary>
	/// Description of ModemLogView.
	/// </summary>
	public class ModemLogView : ISmsReporter
	{
		public string Log {get;set;}
		
		public ModemLogView()
		{
		}
		
		public void LogText(string message)
		{
			Log += String.Format("LogText({0})", message) + "\n";
		}
		
		public void SetSmsReceived()
		{
			Log += "SetSmsReceived()" + "\n";
		}
		
		public void SetSmsSent()
		{
			Log += "SetSmsSent()" + "\n";
		}
		
		public void SetSmsHandled()
		{
			Log += "SetSmsHandled()" + "\n";
		}
		
		public void ClearSmsStatus()
		{
			Log += "ClearSmsStatus()" + "\n";
		}
		
		public void SetLatchValue(float value)
		{
			Log += String.Format("SetLatchValue({0})", value) + "\n";
		}
		
		public void SetAutomaticMode()
		{
			Log += "SetAutomaticMode()" + "\n";
		}
		
		public void SetManualMode()
		{
			Log += "SetManualMode()" + "\n";
		}
		
		public void SetDoorOpenAlarm()
		{
			Log += "SetDoorOpenAlarm()" + "\n";
		}
		
		public void SetControlMotorAlarm()
		{
			Log += "SetControlMotorAlarm()" + "\n";
		}
		
		public void SetEAAlarm()
		{
			Log += "SetEAAlarm()" + "\n";
		}
		
		
		void ISmsReporter.SetNoMoreSms()
		{
			Log += "SetNoMoreSms()" + "\n";
		}
	}
}

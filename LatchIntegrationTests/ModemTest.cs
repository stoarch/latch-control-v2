﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 29.08.2018
 * Time: 11:48
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO.Ports;
using System.Threading;

using FosforLatch.Mocks;
using NUnit.Framework;

namespace FosforLatch.IntegrationTests
{
	[TestFixture]
	public class ModemTest
	{
		ModemLogView modemView_;
		LineHandlerLogView lineView_;
		SerialPort serialPort_;
		
		[Test, Category("Integration test")]
		public void IntegrationTestSendAT()
		{
			//Setup		
			var modem = MakeModemAt("COM3");
		
			//Test
			modem.SendToSerial("AT\r\n");			
			modem.WaitUntilReceived();
			
			Assert.AreEqual("AT| | OK|", modem.FullReceivedLine.TrimEnd());
			//Tear down
			serialPort_.Close();
		}		
				
		Modem MakeModemAt(string comPort)
		{
			modemView_ = new ModemLogView();
			lineView_ = new LineHandlerLogView();
				
			ReceivedLineHandler lineHandler = new ReceivedLineHandler();
		
			serialPort_ = new SerialPort();
			serialPort_.PortName = comPort;
			if(!serialPort_.IsOpen)
			{
				serialPort_.Open();	
				serialPort_.TimedWaitUntilOpened();
			}
			
			Modem modem = new Modem();
			modem.Port = serialPort_;
			modem.SmsReporter = modemView_;
			modem.LineHandler = lineHandler;
			modem.InitPort();

			lineHandler.LineView = lineView_;
			lineHandler.Modem = modem;
			
			return modem;			
		}
	}
}

﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 31.08.2018
 * Time: 10:44
 * 
 */
using System;
using System.IO.Ports;
using System.Threading;

namespace FosforLatch
{
	/// <summary>
	/// Additional methods for SerialPort for waiting and handling
	/// </summary>
	public static class SerialPortExtensions
	{
		public static void TimedWaitUntilOpened(this SerialPort port)
		{
			const int MAX_COUNT = 10;
			int count = 0;
			
			while(!port.IsOpen)
			{
				Thread.Sleep(100);//Make it once and use in all modem tests
				if(count++ > MAX_COUNT)
				{
					throw new TimeoutException("Com port is not opened");
				}
			}
		}
	}
}

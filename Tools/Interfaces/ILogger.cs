﻿/*
 * Created by SharpDevelop.
 * User: stoarch
 * Date: 04.09.2018
 * Time: 10:18
 *
 *  
 */
using System;

namespace Tools.Interfaces
{
	/// <summary>
	/// Description of ILogger.
	/// </summary>
	public interface ILogger
	{
		void LogText(string message);
	}
}
